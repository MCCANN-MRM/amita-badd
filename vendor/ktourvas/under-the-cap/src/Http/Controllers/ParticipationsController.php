<?php

namespace UnderTheCap\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use UnderTheCap\Entities\WinPresent;
use UnderTheCap\Events\ParticipationSubmitted;
use UnderTheCap\Exceptions\ParticipationRestrictionException;
use UnderTheCap\Exceptions\ParticipationsPerDayLimitReachedException;
use UnderTheCap\Exceptions\RedemptionCodeException;
use UnderTheCap\Invokable\InstantWinsManager;
use UnderTheCap\Entities\Participation;
use UnderTheCap\Entities\Promo;
use UnderTheCap\Entities\RedemptionCode;
use UnderTheCap\Entities\Win;
use Log;

class ParticipationsController extends Controller {

    protected $promo;

    public function __construct(Request $request)
    {
        if(!empty($request->utc_env)) {
            \App::make('UnderTheCap\Entities\Promos')->setCurrent($request->utc_env);

            $this->promo = \App::make('UnderTheCap\Entities\Promos')->current();
        }
    }

    public function submitCode(Request $request) {
        return $this->submit($request, 'redemption');
    }

    public function submit(Request $request, $submissionType = 'sole')
    {

        //Start by validating the promo status. Throws PromoStatusException if status !== r
        $this->promo->validatePromoStatus();

        // Validate the form submission. Despite of the type of submission a single set of validation rules is
        // used for convenience.
        $this->validate($request,
            $this->promo->participationValidationRules()->toArray(),
            $this->promo->participationValidationMessages()->toArray()
        );

        /**
         * In the case that participation restrictions have been imposed, check if the submitted can information
         * can create a new participation.
         */
        $this->validateRestrictions($request, $this->promo->info());

        // If the submission type includes code redemption, validate the code of the submission
        $code = null;
        if( $submissionType == 'redemption' && !empty($request->code) ) {
            $code = $this->getRedemptionCode($request->code, $request, $this->promo->info());
            if(empty($code)) {
                throw new RedemptionCodeException();
            }
            
             /* ADDED FOR SECURITY DUPLICATE CHECK 26/02/2021 */

            if($code->reusable == 0) {

                // Get redemptions with the requested code
                $codes = $this->getRedemptionCodes($code->code);

                /* Added 14/4/2021 to limit participations per day */
                $user = $request->user('api');

                if($user == null)
                    throw new RedemptionCodeException();

                if($user->participations()->whereDate('created_at', \Carbon\Carbon::today())->count() >= 2) {
                    throw new ParticipationsPerDayLimitReachedException();
                }
                
                
                if($this->isConsideredFraudParticipation($user,5, $code->code)) {
                    throw new RedemptionCodeException();
                }
                
                /* End  Added 14/4/2021 to limit participations per day */
                
                // Check if a participation exists based on $codes
                $participationsWithRequestedCode = $this->participationsThatRelateWith($codes);

                // If there is at least one participation that relate with the requested code, throw exception
                if (count($participationsWithRequestedCode) > 0) {
                    throw new RedemptionCodeException();
                }
            }

            /* END ADDED FOR SECURITY DUPLICATE CHECK 26/02/2021 */
        }

        $participation = $this->createParticipation($request, $code);

        $participation = $this->playInstant($participation);

        // All went well, emit the ParticipationSubmitted event
        if(!empty($participation)) {
            event( new ParticipationSubmitted($participation) );
        }

        return [
            'success' => !empty( $participation ),
            'participation' => !empty( $participation ) ? $participation : null
        ];

    }

    public function isConsideredFraudParticipation($user, $previousParticipationsTotalToCheck, $newSubmittedCode){
    
            $propabilityPercentageThatIsConsideredFraud = 60;
    
            $latestCodesSubmitted = $user->participations()
                ->whereHas('redemptionCode', function ($query){
                    return $query->where('reusable', '=', 0);
                })
                ->orderBy('created_at', 'desc')
                ->take($previousParticipationsTotalToCheck)
                ->get()
                ->pluck('redemptionCode.code');
    
            if($latestCodesSubmitted->count() == 0)
                return false;
    
    
    //        $propabilityOfFraudTotal = 0;
    
            foreach($latestCodesSubmitted as $latestCodeSubmitted){
    
                $charsDifferential = levenshtein($newSubmittedCode, $latestCodeSubmitted);
    
                if($charsDifferential == 1) {
                    Log::channel('code_submissions_fraud')->debug('User('.$user->id.','.$user->email.','.\Request::ip().') tried to submit code ' . $newSubmittedCode . ' but a previous code('.$latestCodeSubmitted.') found having one letter difference. Latest participations/codes => ' .  implode(",", $latestCodesSubmitted->toArray()));
                    return true;
                }
    
    //            $percentageOfDifference = 100 - (($charsDifferential /  strlen($newSubmittedCode)) * 100);
    //            dump('Comparing ' . $newSubmittedCode . ' with ' . $latestCodeSubmitted . ' probabilitiy is ' .$charsDifferential . ' (percentage' . $percentageOfDifference.')');
    //            $propabilityOfFraudTotal += $percentageOfDifference;
            }
    
    //        $propabilityOfFraudTotal /= $previousParticipationsTotalToCheck;
    
    
    
    //        if($propabilityOfFraudTotal >= $propabilityPercentageThatIsConsideredFraud) {
    //            Log::channel('code_submissions_fraud')->debug('User('.$user->id.','.$user->email.') tried to submit code ' . $newSubmittedCode . ' but probability of fraud was ' .  $propabilityOfFraudTotal . ' based on ' . $previousParticipationsTotalToCheck . ' latest participations/codes => ' .  implode(",", $latestCodesSubmitted->toArray()));
    //            return true;
    //        }
    
            return false;
        }
    
    /**
     * Retrieve a redemption code by code
     * @param $code
     * @return mixed
     */
    public function getRedemptionCode($code, $request, $info) {

        return RedemptionCode::where('code', $code)->where(function($q) use ($info, $request) {

            $q->whereDoesntHave('participation')
//                ->orWhere('reusable', 1)
                ->orWhere(function($q) use ($info, $request) {
                    $q->where('reusable', 1);
                    if( !empty($info['participation_restrictions'])
                        && $info['participation_restrictions']['allowed_frequency'] == 'daily_reusable_code' ) {
                        $q->whereDoesntHave('participation', function($q) use ($info, $request) {
//                            $q->where('emai');
                            foreach ($info['participation_restrictions']['identify_by'] as $identifier) {
                                $q->where( $identifier, $request->get($identifier) );
                            }

                            $q->whereDate( 'created_at', date('Y-m-d') );

                        });
                    }
                })
            ;

        })->first();

    }

    /**
     * @param $request
     * @param $info
     * @throws ParticipationRestrictionException
     */
    public function validateRestrictions( $request, $info ) {
        if( !empty( $info['participation_restrictions'] ) ) {
            $count = 0;
            if($info['participation_restrictions']['allowed_frequency'] == 'daily') {
                $count = Participation::where(function($q) use ($request, $info) {
                    foreach ($info['participation_restrictions']['identify_by'] as $identifier) {
                        $q->where( $identifier, $request->get($identifier) );
                    }
                })
                    ->where(function($q) {
                        $q->whereDate( 'created_at', date('Y-m-d') );
                    })
                    ->count();
            }

            if($count >= $info['participation_restrictions']['allowed_number'] ) {
                throw new ParticipationRestrictionException();
            }

        }
    }

    /**
     * Run the instant win invokable and attach wins, if any, to the participation
     * @param Participation $participation
     * @return Participation
     * @throws \UnderTheCap\Exceptions\PromoConfigurationException
     */
    public function playInstant(Participation $participation) {

        if( $this->promo->instantDraws()->count() > 0 ) {

            $instant = new InstantWinsManager();

            foreach( $this->promo->instantDraws() as $id => $info ) {

                if( !empty($info['restrict_wins_by']) ) {
                    if(
                        Participation::whereHas('win', function($q) use ($id, $info) {
                            $q->where('type_id', $id);
                        })
                            ->where(function($q) use ($participation, $info) {
                                if(is_array($info['restrict_wins_by'])) {
                                    foreach ($info['restrict_wins_by'] as $field ) {
                                        $q->where($field, $participation[$field]);
                                    }
                                } else {
                                    $q->where($info['restrict_wins_by'], $participation[$info['restrict_wins_by']]);
                                }
                            })
                            ->count() != 0)
                    {
                        continue;
                    }
                }

                $win = $instant($id, $info, $participation);

                if( $win !== false) {

                    $pwin = $participation->win()->create([
                        'type_id' => $id,
                        'present_id' => $win['id'],
                        'confirmed' => (!empty($info['auto_approved']) &&  $info['auto_approved'] === true) ? 1 : 0
                    ]);

                    $winpresent = $pwin->winpresent()->create([
                        'present_id' => $win['id']
                    ]);

                }

            }

            $participation->load('win.winpresent.present');

            if( $participation->win()->exists() ) {

                $participation->win[0]->winpresent->present->load(['variants' => function($q) {
                    $q->where( 'remaining', '>', 0);
                }]);

                if( count($participation->win[0]->winpresent->present->variants) == 1 ) {

                    $participation->win[0]->winpresent->update([
                        'variant_id' => $participation->win[0]->winpresent->present->variants[0]->id
                    ]);

                    $participation->win[0]->winpresent->present->variants[0]->update([
                        'remaining' =>  \DB::raw(' remaining - 1 ')
                    ]);

                }

            }

        }
        return $participation;
    }

    /**
     * Create and return a new Participation.
     *
     * @param $request
     * @param null $code
     * @return Participation
     */
    private function createParticipation($request, $code = null) {

        // Get the participation create associative array
        $create = $this->participationCreateArray($request);

        // Create a new participation associated with the user if there is one
        $participation = $request->user('api') === null ?
            Participation::create($create) :
            $request->user('api')->participations()->create($create);

        // If a code has been validated, assign it to the participation
        if( !empty($code) ) {
            
            $participation->name    = $request->user('api')->name;
            $participation->email   = $request->user('api')->email;
            $participation->user_id = $request->user('api')->id;
            
            $participation->redemptionCode()->associate($code);
            $participation->save();
        }

        return $participation;
    }

    /**
     * Create the associative array to be used for participation creation
     * @param $request
     * @return array
     */
    private function participationCreateArray($request) {
        // Filter the promo participation fields excluding the code and anything else needed in future updates
        $fields = collect($this->promo->participationFieldKeys())->reject(function ($field) {
            return $field === 'code';
        })
            ->map(function ($field) {
                return $field;
            });

        // Create the array for participation creation and feed it to the model create method
        $create = [];
        foreach ( $fields as $field) {
            $create[$field] = $request->get($field);
        }
        return $create;
    }


    public function truncate() {
        if(!\App::environment('production')) {
            WinPresent::truncate();
            Win::truncate();
            Participation::truncate();
        }
    }
    
        /* ADDED FOR SECURITY DUPLICATE CHECK 26/02/2021 */
    public function getRedemptionCodes($code){
        return RedemptionCode::where('code', $code)->pluck('id');
    }
    /**
     * Retrieve a redemption code by code
     * @param $code
     * @return mixed
     */
    public function participationsThatRelateWith($redemptionCodeIds) {

        return Participation::whereIn('code_id', $redemptionCodeIds)->pluck('id');

    }
    /* END ADDED FOR SECURITY DUPLICATE CHECK 26/02/2021 */
    
      public function removeDuplicates(){
          
          dd("blocked");
        ini_set('max_execution_time', 300);
 
        $shouldExportAsCsv = true;
        $emailFilter = array();

        $emailsThatShouldNotRemoveCodes = array('geovelalis@gmail.com', 'skarl.kostas@gmail.com', 'panayopetour@gmail.com', 'geoxrys@gmail.com', 'antonistsimourtos@gmail.com');
        $emailsThatShouldDirectlySetForZeroPoints = array();

        $duplicateCodesNotUsedTwice = new \Illuminate\Support\Collection();
        
//
//        $redemptionCodesDuplicatesWithParticipations = RedemptionCode::whereIn('code', function ( $query ) {
//            $query->select('code')->from('motion_liftmeup_redemption_codes')->groupBy('code')->havingRaw('count(code) > 1');
//        })->get();
//
//        dd($redemptionCodesDuplicatesWithParticipations);

        $codesList6Digit = array(
            'A6P773',
            'A4P773',
            'AA3773',
            'AAE773',
            'AAF773',
            'AAU3KZ',
            'AB3JUZ',
            'ABU9UZ',
            'ABUJ0N',
            'ABUJUN',
            'ABUPUN',
            'ADU3KZ',
            'ADUJUZ',
            'AJUJUC',
            'AKU3KZ',
            'APUJUX',
            'APUJUZ',
            'APUSUZ',
            'AUUJUX',
            'AXUJUN',
            'A1U3KZ',
            'AAUJUC',
            'AB2JUZ',
            'ABFJUN',
            'ABJJUZ',
            'ABUJ3N',
            'ABUJ4Z',
            'ABUJNZ',
            'ABUJUC',
            'ABUJYN',
            'ADOJUZ',
            'ADU3EZ',
            'ADU7UZ',
            'ADUDUZ',
            'ADULUJ',
            'ADULUS',
            'APUJMZ',
            'APUJVZ',
            'AUUJUZ',
            'AUJ52V',
            'ANF49M',
            'ANF68A',
            'ANJ42U',
            'ANJ92R',
            'ANY43X',
            'AUJ92A',
            'ABK15S',
            'ANA07W',
            'ANA38W',
            'ANA51W',
            'ANA77A',
            'ANF05S',
            'ANF59A',
            'ANF90M',
            'ANF94K',
            'ANG05W',
            'ANG07J',
            'ANG12Y',
            'ANG16B',
            'ANG19B',
            'ANG22B',
            'ANG24Z',
            'ANG35B',
            'ANG40Z',
            'ANG43W',
            'ANG47U',
            'ANG52H',
            'ANG66B',
            'ANG70M',
            'ANG81K',
            'ANG85H',
            'ANG90E',
            'ANG93K',
            'ANH50J',
            'ANH76J',
            'ANJ00A',
            'ANJ03W',
            'ANJ03X',
            'ANJ04W',
            'ANJ06Z',
            'ANJ11W',
            'ANJ15B',
            'ANJ15S',
            'ANJ16Z',
            'ANJ22S',
            'ANJ28A',
            'ANJ45C',
            'ANJ46C',
            'ANJ48Q',
            'ANJ50J',
            'ANJ68B',
            'ANJ95V',
            'ANJ99H',
            'ANY04Q',
            'ANY18Z',
            'ANY19S',
            'ANY20X',
            'ANY21F',
            'ANY24Z',
            'ANY30X',
            'ANY45G',
            'ANY47S',
            'ANY56F',
            'ANY61W',
            'ANY62X',
            'AUJ03W',
            'AUJ25A',
            'AUJ28A',
            'AUJ41F',
            'AUJ51X',
            'AUJ64H',
            'AUJ66V',
            'AUJ72G',
            'AUJ73A',
            'AUJ77W',
            'AUS03H',
            'AUS07A',
            'AUS09H',
            'AUS15Y',
            'AUS26Y',
            'AUS55E',
            'AUS75S',
            'AYG34S',
            'ABK20S',
            'ABK39S',
            'AFQ5XP',
            'AGE57S',
            'ANA93A',
            'ANF06K',
            'ANF15K',
            'ANF38K',
            'ANF69A',
            'ANF98S',
            'ANG05Q',
            'ANG20U',
            'ANG23Z',
            'ANG25Z',
            'ANG32M',
            'ANG47Q',
            'ANG49B',
            'ANG55U',
            'ANG58U',
            'ANG64B',
            'ANG64U',
            'ANG79A',
            'ANG80G',
            'ANG86Z',
            'ANG93Y',
            'ANJ01W',
            'ANJ04R',
            'ANJ23V',
            'ANJ28R',
            'ANJ33A',
            'ANJ34G',
            'ANJ36Z',
            'ANJ37W',
            'ANJ41A',
            'ANJ46Z',
            'ANJ60S',
            'ANJ68Z',
            'ANJ76A',
            'ANJ77Y',
            'ANJ78J',
            'ANJ79V',
            'ANJ82B',
            'ANY07S',
            'ANY10G',
            'ANY16C',
            'ANY32S',
            'ANY36F',
            'ANY39G',
            'ANY47W',
            'ANY52W',
            'ANY58F',
            'ANY71F',
            'ANY90G',
            'AUJ00A',
            'AUJ02H',
            'AUJ14V',
            'AUJ21G',
            'AUJ31S',
            'AUJ36X',
            'AUJ39G',
            'AUJ47S',
            'AUJ47X',
            'AUJ62G',
            'AUJ67A',
            'AUJ67S',
            'AUJ76S',
            'AUJ81H',
            'AUJ81X',
            'AUJ86X',
            'AUS01J',
            'AUS03S',
            'AUS03W',
            'AUS06E',
            'AUS13W',
            'AUS38E',
            'AUS51S',
            'AUS52W',
            'AUS56H',
            'AUS62H',
            'AUS66L',
            'AUS06X',
            'AUX38Q',
            'AHV93X',
            'AUJ27K',
            'AUK28L',
            'A7KVID',
            'ABH03Q',
            'ABH25Q',
            'ABH29L',
            'ABH83L',
            'AHV30S',
            'AHV49Q',
            'AIH27Q',
            'AIH62Q',
            'AIH80Q',
            'AIX11Q',
            'ANB43S',
            'ANG06I',
            'ANG14A',
            'ANG14R',
            'ANG24V',
            'ANG57V',
            'ANG74V',
            'ANG98I',
            'ANH24Q',
            'ANH24W',
            'ANH33Q',
            'ANH42W',
            'ANH54X',
            'ANS02J',
            'ANS93Z',
            'ANS95J',
            'AUC18U',
            'AUC31U',
            'AUC37Y',
            'AUC41U',
            'AUC44Y',
            'AUC76Y',
            'AUG03R',
            'AUG33R',
            'AUG63Y',
            'AUG72Y',
            'AUG88H',
            'AUJ01Q',
            'AUK01Z',
            'AUK02W',
            'AUK08W',
            'AUK09X',
            'AUK11V',
            'AUK22X',
            'AUK34L',
            'AUK51W',
            'AUK58V',
            'AUK62Z',
            'AUK67V',
            'AUK69V',
            'AUK83Q',
            'AUS10X',
            'AUS33X',
            'AUX03N',
            'AUX04U',
            'AUX24M',
            'AUX25Y',
            'AUX33Z',
            'AUX46V',
            'AUX49Z',
            'AUX73Y',
            'AUX83Y',
            'AUX90V',
            'AVX20X',
            'AVX23Z',
            'AVX39X',
            'AVX50X',
            'AVX82Q',
            'AWI08G',
            'AWI09P',
            'ABD78Q',
            'ABD85Q',
            'ABH17Q',
            'ABH92L',
            'AHV04W',
            'AHV14Q',
            'AHV33Q',
            'AHV55W',
            'AHV67S',
            'AHV72W',
            'AHV73Q',
            'AIH44Q',
            'AIH65Q',
            'AIH98Q',
            'AJX25Q',
            'AJX59Q',
            'AJX73Q',
            'ANB10S',
            'ANB66S',
            'ANG03P',
            'ANG07I',
            'ANG09R',
            'ANG11V',
            'ANG13A',
            'ANG28V',
            'ANG40R',
            'ANG40V',
            'ANG53I',
            'ANH09W',
            'ANH09X',
            'ANH34Q',
            'ANH41W',
            'ANH73X',
            'ANS66J',
            'AUC35Y',
            'AUC43Y',
            'AUC4QS',
            'AUC66Y',
            'AUC88Y',
            'AUC89Y',
            'AUG05H',
            'AUG09R',
            'AUG10P',
            'AUG18Y',
            'AUG26L',
            'AUG48P',
            'AUG76Y',
            'AUJ31K',
            'AUJ53K',
            'AUJ62K',
            'AUJ96G',
            'AUK08L',
            'AUK08Q',
            'AUK16V',
            'AUK16Z',
            'AUK37W',
            'AUK39X',
            'AUK49W',
            'AUK52L',
            'AUK61X',
            'AUK69W',
            'AUK78Q',
            'AUS19F',
            'AUS30X',
            'AUX01Z',
            'AUX19Y',
            'AUX43Y',
            'AUX48L',
            'AUX58L',
            'AUX75L',
            'AVX04Z',
            'AVX37W',
            'AVX55X',
            'AVX63Q',
            'AVX76Q',
            'AVX88X',
            'AVX88Z',
            'AVX89Z',
            'ARNUWZ',
            'AWUNB7',
            'A48Q9I',
            'AGHJGH',
            'AHGIG6',
            'AHGYUJ',
            'AKTNZW',
            'AKUWBQ',
            'AUNB84',
            'AWJQVZ',
            'AS01RE',
            'AS19UD',
            'AAGVRK',
            'A2YIXS',
            'A0Y7LX',
            'A1Y5DX',
            'A1Y6GB',
            'A1Y9SB',
            'A2Y0FG',
            'A2Y4PC',
            'A2Y8DX',
            'A3Y4GB',
            'A3Y5GB',
            'A3Y6JV',
            'A3Y7TV',
            'A3Y9FG',
            'A4Y1DX',
            'A4Y2DX',
            'A4Y4LX',
            'A5Y8BC',
            'A5YOXL',
            'A6Y1DX',
            'A6Y2XL',
            'A6Y3HD',
            'A6Y7MI',
            'A6Y8HD',
            'A7Y9JV',
            'A8Y0FG',
            'A9Y4HD',
            'A9YWXL',
            'AF46LW',
            'A1Y1JV',
            'A1Y1MI',
            'A1Y2PC',
            'A1Y3BC',
            'A1Y7TV',
            'A2Y6MI',
            'A2Y9KB',
            'A3Y0GB',
            'A4Y5JV',
            'A4Y8XL',
            'A5Y2DX',
            'A5Y2PC',
            'A5Y3JV',
            'A6RVKB',
            'A6Y9GC',
            'A7Y1SB',
            'A7Y9XL',
            'A8Y2HD',
            'A8Y3XL',
            'A9Y2JV',
            'A9Y3HD',
            'A9Y5GB',
            'A9Y9FG',
            'A6E93C',
            'AFEZTC',
            'AJRQER',
            'AQ4V7C',
            'A0QOU0',
            'AA0Y8O',
            'AF88YY',
            'AFCKR7',
            'AHIFVO',
            'AHMIP7',
            'AQHJ3G',
            'AUQ8C0',
            'AB7VNR',
            'AG0WZ4',
            'AU47GA',
            'ACIQSX',
            'AEAXMJ',
            'AJUT3E',
            'A8UCKE',
            'A8ZGBD',
            'AJ95KH',
            'A6NSXG',
            'AYFJ5I',
            'A21HA1',
            'A2GXSJ',
            'AOJDBP',
            'AF3BRG',
            'AC55M7',
            'AL21HO',
            'AKGS5A',
            'AHYQNL',
            'A8K7AC',
            'AFD816',
            'AKO9IU',
            'AQW5QT',
            'AWIE3S',
            'A2CXN7',
            'A8JAMW',
            'AU9OSP',
            'A3XHDD',
            'AGXOSI',
            'A2DLRV',
            'AAKCF7',
            'AM2M73',
            'ACJ3US',
            'AQK2AR',
            'AQK5AR',
            'AQW1EF',
            'AQY5FS',
            'AUC12T',
            'AUGUES',
            'AUH44R',
            'AUH66L',
            'AUX33I',
            'AUX40I',
            'AUX78I',
            'AWU1MS',
            'AWY9DY',
            'AX4W84',
            'AYCF7U',
            'AQ07EF',
            'AT79LT',
            'AEW9OH',
            'AKS1VZ',
            'ANG75P',
            'ANGARA',
            'AQD1TP',
            'AQD6TP',
            'AQK3AS',
            'ASD9FZ',
            'AUH42S',
            'AUS56Q',
            'AWD9MI',
            'AWY9DS',
            'AEW8PB',
            'APH23D',
            'ASDXV8',
            'ATUYYE',
            'AJN8E2',
            'ANHU1K',
            'AIJ7JS',
            'AWZSH2',
            'A3OYW1',
            'AT9TBM',
            'AU3E43',
            'A0YVYG',
            'A7B4SX',
            'A9KX9M',
            'AB6DVQ',
            'AET6YE',
            'AH8GPZ',
            'APKNVQ',
            'AQEGLL',
            'AX4G7K',
            'A6XQ22',
            'AIKFN1',
            'AOUE9A',
            'AS2UVU',
            'A7APSP',
            'ANVNSB',
            'AXSYCK',
            'APUD9Y',
            'ADLWUG',
            'AT8NC5',
            'AXVFVU',
            'AQ858J',
            'AJUZH9',
            'AC4FHJ',
            'ATW15H',
            'AVS494',
            'A0QP9C',
            'AY4YAU',
            'AO6L7K',
            'AR51RQ',
            'A7CZOV',
            'ADCX85',
            'AHWYS2',
            'ASHBDR',
            'ATRGI5',
            'AQ2A8R',
            'AHZ0S0',
            'AMM716',
            'AX6T9M',
            'A2KFCA',
            'AT9GHG',
            'A6OA44',
            'AVU9YO',
            'A7RQO2',
            'A94UVQ',
            'ALICTT',
            'AGKSA2',
            'AUTW65',
            'A2UWCA',
            'ABWRIV',
            'AK52TL',
            'ASN40A',
            'AZBK0P',
            'A334HJ',
            'ALSF98',
            'AGXGRP',
            'AZ13TD',
            'A97OJB',
            'APJPVE',
            'AWD5IC',
            'AR7SHO',
            'A18AA1',
            'A4MY6X',
            'A38JOB',
            'A6TQOP',
            'ATYX4C',
            'AP9GNT',
            'A5ULBJ',
            'ANHTXK',
            'A40XHN',
            'ACZFRL',
            'AOV5OO',
            'AZ8JZE',
            'AFK4VD',
            'AWKBBL',
            'AKRTD2',
            'ADEF6N',
            'A9KD6G',
            'AFRO9F',
            'AUE6F6',
            'AOJI6K',
            'AI3XL9',
            'AYJDK9',
            'ANEL4G',
            'AYXL22',
            'A865IQ',
            'A7SNGQ',
            'AOZZXY',
            'A4EIH6',
            'A63NJ9',
            'ATY9AR',
            'AF5FV5',
            'AMDZMV',
            'ACBC2N',
            'ATJ55M',
            'AL26KI',
            'A0V7H2',
            'ANG58Y',
            'ASPMC4',
            'AV9N3C',
            'AJ4OZC',
            'A8WX95',
            'AKO0NS',
            'AKM3L8',
            'A8COXA',
            'A2KV43',
            'AN25C9',
            'AVL48A',
            'AR2WYB',
            'A1AAMP',
            'A95T4V',
            'AEKXT6',
            'AN7ZUZ',
            'AQMCX3',
            'ATFF94',
            'A11C01',
            'ANE2A6',
            'AWOOY0',
            'AABK3R',
            'AGYX8P',
            'A3STN9',
            'AX14A6',
            'A6HS38',
            'AW5FIO',
            'A2ZULP',
            'AUV6S0',
            'A3SUVZ',
            'A0GW18',
            'A7RB0H',
            'AOWK85',
            'AVJ7MQ',
            'AEVZX1',
            'A4N8OP',
            'A7QGGY',
            'ATWCT9',
            'AURVCI',
            'AH21HK',
            'A0FH1T',
            'A0Y1DV',
            'AU67C8',
            'AN6PP5',
            'AUS939',
            'ARVP56',
            'ANUIHJ',
            'AT5JD9',
            'AOQLZL',
            'AJGU0C',
            'AT7MOW',
            'ATKKFF',
            'ARMLQD',
            'A6U28B',
            'A8NLYZ',
            'AHY79S',
            'AID3LK',
            'A12345',
            'AAA7I1',
            'A0Q7I8',
            'A3ZNWB',
            'A6VVYG',
            'AXEYC3',
            'A5BMWG',
            'ABASXN',
            'ACJF1E',
            'A8DV5B',
            'AGYQ4V',
            'AL284O',
            'A5NWZU',
            'AT7U1C',
            'ANV8B5',
            'AF6V6P',
            'AMF70O',
            'AOK7OS',
            'AUI5IJ',
            'AZSCYT',
            'ALGLA1',
            'AHFNPB',
            'AVSJR8',
            'AGNO1W',
            'AKK1WC',
            'AUMEKE',
            'AZ86ER',
            'A03TPU',
            'ACPZSU',
            'ANNPLG',
            'A2VXFJ',
            'AL87T3',
            'AVLTJ1',
            'AMIERA',
            'AHVVU6',
            'AZHW39',
            'AAYSVI',
            'AHBULV',
            'AXFLH1',
            'A0Z58D',
            'A57D2T',
            'A62ZOK',
            'A9G119',
            'AB0MZI',
            'AING3X',
            'AK4IZZ',
            'AMI4WH',
            'ANXPLQ',
            'AOJC98',
            'AQVM07',
            'AU2SHI',
            'AVODVH',
            'AWZSCI',
            'AY23HT',
            'AYIPM3',
            'AFS3XD',
            'A2NLWD',
            'AF9URB',
            'A11P8K',
            'A428IP',
            'A71MBA',
            'A8RZ72',
            'A98N5R',
            'ACHFAE',
            'AJF8L9',
            'AKUTD1',
            'ASQTTZ',
            'AUSLT9',
            'A58X7T',
            'AY3K19',
            'AS3ZKU',
            'AF4IXO',
            'A5XQKQ',
            'A4W6R8',
            'AM9BNI',
            'ATRWZR',
            'ARPNU9',
            'AO96MT',
            'AYCZ4S',
            'AF2MHP',
            'A82MZC',
            'A1SYKA',
            'APUA9V',
            'AW7U6B',
            'A1H0JY',
            'APXPMF',
            'A8X0LL',
            'AWQYFT',
            'AL00YT',
            'A1ZCXE',
            'A0VPZZ',
            'A4AF70',
            'A7QMF5',
            'A8E6LJ',
            'AAU4G1',
            'AGQAJL',
            'AHIDL4',
            'AP40E1',
            'ASJVYN',
            'AUQZY2',
            'AYEGNX',
            'AQ8CDX',
            'A8HVNK',
            'ALF7XS',
            'A7L9XJ',
            'A3R3UM',
            'AJX4KI',
            'AARMGK',
            'ABM5W0',
            'ADIMGO',
            'AECSDR',
            'AFZ57V',
            'AGMYSO',
            'APG4ML',
            'AQGPJ0',
            'ASML2L',
            'AH7NUK',
            'A3S2KT',
            'A57GMY',
            'A7BRRF',
            'A7CK3P',
            'A7FT8F',
            'A9GWPA',
            'AEXL8D',
            'AGA14D',
            'AGXDLJ',
            'AJRD30',
            'ALPVGX',
            'AN5BDH',
            'AQ3XPH',
            'AQSRV7',
            'AZ5NY0',
            'AIMTHM',
            'AR4CXM',
            'A4ZWV4',
            'A044MG',
            'A53O3W',
            'A903FT',
            'A9BU6P',
            'A9TSIL',
            'AFTBUQ',
            'AICGZF',
            'AN40X4',
            'AVX9K2',
            'AFH3A0',
            'AL0ZSZ',
            'A7VNM9',
            'A9PDXW',
            'AYKJW2',
            'AU8TO7',
            'A9532M',
            'ABLIWB',
            'AE7422',
            'ABK3G5',
            'A7Z8XK',
            'AH4D6Y',
            'A6GLB9',
            'AU7TO5',
            'AI5IRO',
            'A3VHTD',
            'AZ8JGQ',
            'A2MYBB',
            'A2VF53',
            'ARTOK6',
            'AH1HJI',
            'AEG5IL',
            'AI14SJ',
            'A04EEP',
            'A06PPE',
            'A13PEP',
            'A18PEE',
            'A20EEP',
            'A30LF3',
            'A36PEP',
            'A82PPE',
            'A89PPE',
            'A5VOT6',
            'A6W6IZ',
            'A8ZQPN',
            'AAJ61C',
            'AB4RB8',
            'AHRVRG',
            'ALH309',
            'ARSOW4',
            'AUD1JF',
            'AUQVFW',
            'AYVRVX',
            'A9GEVH',
            'ATO3XE',
            'A4QD6I',
            'ADCCLE',
            'AERXOB',
            'AI71GH',
            'AMLEKG',
            'A4JHQC',
            'A09PPE',
            'A29PPE',
            'A52PPE',
            'A56PEP',
            'A64PEE',
            'A96PEE',
            'AUS06J',
            'A4SQS8',
            'AB0LK6',
            'AM08IL',
            'AP7GXK',
            'AU9RSN',
            'AKL98G',
            'AV6VOH',
            'ALSHIE',
            'AMNTXZ',
            'ARO1RI',
            'AUL6KU',
            'AI63KU',
            'AC27ZP',
            'AG1L9C',
            'A8GSFB',
            'A6DXNY',
            'AY2FXB',
            'ANV1JZ',
            'AKSFNG',
            'A16STH',
            'A4SDMF',
            'AOCDQN',
            'A8FBX9',
            'A3PMTM',
            'AO85M5',
            'AKSSS4',
            'A28BMY',
            'ABEBJD',
            'AH5MR9',
            'AHKBTQ',
            'AXIQUI',
            'APJIAN',
            'AUS3G5',
            'A3PB42',
            'AMAV7N',
            'ASH7ZK',
            'AQ7FLR',
            'A0KHQQ',
            'ADULBV',
            'A78POG',
            'AKCADQ',
            'ABV11N',
            'AWPM0D',
            'AJGBR9',
            'ASM3NM',
            'A1FHHX',
            'AFJIX7',
            'AHFNG5',
            'ALGOH3',
            'AQGBO2',
            'ATHON3',
            'AVDIGJ',
            'AVKFW8',
            'AZBVI9',
            'ASX76W',
            'ATNOVW',
            'AXPC7O',
            'A5B1VC',
            'ACKRZ7',
            'AT36F5',
            'AB2B4V',
            'AWW5BA',
            'AFJIV2',
            'ANCOT6',
            'ANGUD3',
            'ANOJE5',
            'ARING8',
            'ARTIV2',
            'ATING6',
            'AUCPW1',
            'A25Z47',
            'APOBOY',
            'AAH5M0',
            'AK5BLS',
            'A7EHAW',
            'A9ARYK',
            'ANTZ0D',
            'AFIZXR',
            'A1U7EM',
            'A3O24M',
            'A9C3XA',
            'AJHLKY',
            'AJKZPX',
            'AL0LUE',
            'ASFHU7',
            'AV4384',
            'AXP35J',
            'ABOYCG',
            'A3AXR3',
            'AXYE5R',
            'A3AJG2',
            'A3AKL6',
            'A3AOS3',
            'A3AVN6',
            'A3AWH8',
            'A3AZM3',
            'A5HY68',
            'AAF6M8',
            'ACL4QD',
            'AJGFCX',
            'AZ3UVW',
            'A8RTHH',
            'A12345',
            'A0C11D',
            'APA4DK',
            'AZVOZ2',
            'A0YQMZ',
            'A3AJD9',
            'A3AKL4',
            'A3APT6',
            'A3AWP7',
            'A3PLS2',
            'AHJ5E3',
            'AUBCVW',
            'AJ7QJH',
            'A3DZCY',
            'ACI6K8',
            'AH4GSJ',
            'AVC9F5',
            'AITJ12',
            'AERZS4',
            'ATYC2V',
            'A9A8GR',
            'AAI4JV',
            'AZLNKG',
            'AH5SNL',
            'A8X38X',
            'AMQF2W',
            'AOWNGN',
            'AQRF3M',
            'AMJZJP',
            'AAP184',
            'AAP892',
            'AAP928',
            'AF7JUZ',
            'AKHY5F',
            'ABTT6G',
            'AFV7NV',
            'AMFV7G',
            'ASTRDK',
            'ABMRF9',
            'AO198J',
            'ATF4SY',
            'A2EMU4',
            'A3FLH6',
            'AX365Z',
            'AA34BI',
            'AA39EI',
            'AAP128',
            'AAP158',
            'AAPB86',
            'AD7JU5',
            'AD7JU7',
            'AD7JUS',
            'AUXR02',
            'A5NBT4',
            'A7Z8CR',
            'A006P7',
            'A220U7',
            'A261AS',
            'A8JU9X',
            'A950CK',
            'AEOT6H',
            'AFMP2R',
            'AJE8U1',
            'AKYWBI',
            'ASO4AN',
            'AWKLXB',
            'ATXBG7',
            'AXATTF',
            'ANKZEU',
            'AXSNCC',
            'AZ90DW',
            'AWPEMQ',
            'AUX88G',
            'A104TT',
            'AYNFK5',
            'ABH00W',
            'ABH13S',
            'AUS74H',
            'AUX08X',
            'AUX17X',
            'AUX29X',
            'AUX47W',
            'AUX51J',
            'AUX55G',
            'AUX77X',
            'AUX78S',
            'A9GOAX',
            'AY8T6M',
            'A8GNOY',
            'ADSYA0',
            'AERXMU',
            'AP8I6R',
            'ARF5ZB',
            'AX2XM0',
            'AJRNZJ',
            'AJZWH7',
            'AQHRHI',
            'AQZJJV',
            'AQZYFZ',
            'ATN7FN',
            'AA4Q3T',
            'ABH01S',
            'ABH04S',
            'ABH55W',
            'AUS71H',
            'AUX06C',
            'AUX15C',
            'AUX15J',
            'AUX20C',
            'AUX20R',
            'AUX35W',
            'AUX41C',
            'AUX41R',
            'AUX43S',
            'AUX46J',
            'AUX54G',
            'AUX55J',
            'AUX76C',
            'AUX81R',
            'AUX85R',
            'AUX87R',
            'AUX89W',
            'AUX92S',
            'AUX98J',
            'ARSEWY',
            'A877P7',
            'AY5N6F',
            'A1Q8PO',
            'A8O8ZB',
            'AQKTWY',
            'ASDFG7',
            'AZNFHJ',
            'AZXCV3',
            'AJK7S6',
            'AK92M9',
            'AM19HE',
            'A9M6SU',
            'AEX19I',
            'AUC50N',
            'AUC30Z',
            'AUC63V',
            'AUC03J',
            'AUC05H',
            'AUC05W',
            'AUC23B',
            'AUC30F',
            'AUC37G',
            'AUC65G',
            'AUC71N',
            'AUC74F',
            'AUC79G',
            'AUC89U',
            'AWI34L',
            'AWX15K',
            'AWX88F',
            'ARMH9B',
            'A0F9F3',
            'A5HGOB',
            'A6E7QT',
            'A9QHW4',
            'ABV7J4',
            'AGFY7E',
            'AK5BO9',
            'AQZVM1',
            'AWS0F1',
            'A2VQVV',
            'A3PAOC',
            'A9IQL2',
            'ABNUJ8',
            'A55GUM',
            'A69AKU',
            'ACXAPB',
            'ACWRS8',
            'ABH35X',
            'ABH37X',
            'AEX11M',
            'AEX16G',
            'AEX20N',
            'AEX20R',
            'AEX21Q',
            'AEX25Q',
            'AIW05W',
            'AIW66Y',
            'AIX23D',
            'AIX25X',
            'AIX36K',
            'AIX76U',
            'AIX76V',
            'ANM10S',
            'ANM85S',
            'AUC03C',
            'AUC04Q',
            'AUC06B',
            'AUC12Z',
            'AUC15N',
            'AUC19D',
            'AUC20S',
            'AUC27A',
            'AUC33X',
            'AUC36Z',
            'AUC38T',
            'AUC41H',
            'AUC42A',
            'AUC44S',
            'AUC46G',
            'AUC47X',
            'AUC51S',
            'AUC58A',
            'AUC60R',
            'AUC68D',
            'AUC68T',
            'AUC71H',
            'AUC74E',
            'AUC89N',
            'AUC94X',
            'AUHARX',
            'AWI28N',
            'AWI28Z',
            'AWI34C',
            'AWI39K',
            'AWI51W',
            'AWX00X',
            'AWX02A',
            'AWX09S',
            'AWX09T',
            'AWX11A',
            'AWX12H',
            'AWX12I',
            'AWX16J',
            'AWX25Y',
            'AWX31Q',
            'AWX34S',
            'AWX42Y',
            'AWX45F',
            'AWX48F',
            'AWX48X',
            'AWX55T',
            'AWX62W',
            'AWX71S',
            'AWX75J',
            'AWX80A',
            'AWX81H',
            'AWX85L',
            'AWX94H',
            'AWX97F',
            'A07JYX',
            'A18H9B',
            'AV0P5S',
            'AM1R53',
            'AR997Y',
            'AMM5F6',
            'AY7E2L',
            'A60RVH',
            'AO2E0Z',
            'A0CVLE',
            'A42VGL',
            'AEHCPW',
            'AXOVK4',
            'AZTIKJ',
            'A9AIXU',
            'A0PAET',
            'AUCF05',
            'AVOWPG',
            'AN6LZA',
            'A9QUJH',
            'ALKVUF',
            'A6HXC3',
            'ADGFAS',
            'AZSMKE',
            'A6UZUW',
            'AE0UVF',
            'A4HUXS',
            'A1KRM7',
            'AR53UM',
            'ARNW32',
            'AT0S68',
            'AUMN12',
            'AC4EJT',
            'AHPDNN',
            'A55G1M',
            'A55G6M',
            'ARR22Q',
            'ARRA5Q',
            'AVAG1Y',
            'A7NT8H',
            'AME5KJ',
            'AFZ68X',
            'A01QI6',
            'A8MJUI',
            'AGPEIY',
            'A5STSG',
            'A6SFYA',
            'ARM9FS',
            'AEX12D',
            'AEX12S',
            'AEX12X',
            'AEX13K',
            'AEX15N',
            'AEX17E',
            'AEX18J',
            'AEX18T',
            'AIW06K',
            'AIW06W',
            'AIW21W',
            'AIX23U',
            'AIX24E',
            'AIX36U',
            'AIX76A',
            'AIX76M',
            'ANGAUH',
            'ANM32V',
            'ANM69S',
            'AUC07S',
            'AUC08R',
            'AUC26F',
            'AUC29T',
            'AUC31N',
            'AUC37V',
            'AUC58H',
            'AUC67V',
            'AUC74A',
            'AUC94E',
            'AUC97E',
            'AUC99Q',
            'AUX53B',
            'AUX80B',
            'AWI01H',
            'AWI27H',
            'AWI29I',
            'AWI30A',
            'AWI33U',
            'AWI37X',
            'AWI38I',
            'AWI39D',
            'AWI41J',
            'AWI45M',
            'AWI48O',
            'AWI48Z',
            'AWI49O',
            'AWI49X',
            'AWI51G',
            'AWI54F',
            'AWI54Z',
            'AWX14N',
            'AWX15X',
            'AWX16Y',
            'AWX24K',
            'AWX24P',
            'AWX27Q',
            'AWX33K',
            'AWX35N',
            'AWX36F',
            'AWX41S',
            'AWX46U',
            'AWX53X',
            'AWX59Y',
            'AWX66N',
            'AWX72T',
            'AWX76J',
            'AWX83K',
            'AWX88W',
            'AWX95K',
            'AWX96R',
            'ABG0OE',
            'ABT2PH',
            'AD7JNJ',
            'AF41ZI',
            'AIDFOH',
            'AKGBPJ',
            'AL5X04',
            'ANAV8H',
            'AOK062',
            'AQUV80',
            'AVANKK',
            'AW18BD',
            'AX3QHU',
            'AXQHYN',
            'AVZFQI',
            'A99U48',
            'AY4EF1',
            'A9F6DK',
            'ADRF9P',
            'AFR21U',
            'A5MWJS',
            'AX1DPG',
            'APOEYZ',
            'AWNGVA',
            'ABMP9S',
            'ACHBPZ',
            'APQE1R',
            'A4NW99',
            'AQQTIJ',
            'AMURTM',
            'ADVA9W',
            'AB9AM0',
            'AHFS8N',
            'ARH88T',
            'ASHP43',
            'A74DGI',
            'AIH42N',
            'AY4K8X',
            'AF5PIN',
            'AXDPEY',
            'A0DH74',
            'AWHLPM',
            'ABBC8X',
            'ADSKIX',
            'ADFOUN',
            'AESD34',
            'ASOTYV',
            'ACQJ2G',
            'A4DPVX',
            'A4WFVA',
            'AHUP8F',
            'ATWJRH',
            'AM4BRM',
            'ALQMTH',
            'AHIEGA',
            'ANO99K',
            'A4JQPG',
            'AEW3CK',
            'A7GJIK',
            'A2XSVW',
            'A6KAUV',
            'AC8YF8',
            'AF4UZV',
            'AO82GF',
            'AU0AIQ',
            'AX4OP3',
            'A20H1X',
            'AFFWXH',
            'AS8DE5',
            'A2Z6MX',
            'AWREZZ',
            'A0O6X7',
            'A2K4HG',
            'AANFHL',
            'AEFHLJ',
            'AFJDY6',
            'AGXUOU',
            'A746SB',
            'A1J27U',
            'ASDFAT',
            'ATOSYV',
            'AZOTYV',
            'AZVYLK',
            'AATOBP',
            'ARNPN9',
            'ASSG3N',
            'A1JUMT',
            'A4WLOB',
            'A7GKLK',
            'A7U7KJ',
            'ATLIZ1',
            'AUDT5Y',
            'ADGXXW',
            'A0Y8WS',
            'A1M5GL',
            'A3Y8XG',
            'A5H4GL',
            'A6M5GL',
            'A7M4GL',
            'A8Y5WS',
            'A9M1GL',
            'AE3Q54',
            'A6FGCF',
            'A8IASN',
            'ACKPL8',
            'AIV2XK',
            'ALQX2Q',
            'AOI8MC',
            'AUZ76C',
            'AZNHTA',
            'A77LT6',
            'AD3EAB',
            'ADP5EZ',
            'AJ24P2',
            'AJA89Q',
            'APRCSX',
            'ARX77D',
            'A96BQV',
            'A27646',
            'A688Z5',
            'AQNYOE',
            'AD3XM8',
            'A0Y6WS',
            'A2M6GL',
            'A56A7M',
            'A5Y2WS',
            'A6H3GL',
            'A7Y2WS',
            'A9M1GK',
            'A9M5GK',
            'A9M6GL',
            'AE6K68',
            'AE6Q24',
            'AE6Q54',
            'AE6Y59',
            'AFCB3R',
            'AHDP6E',
            'AK4A9C',
            'AK4A9L',
            'AQCZF1',
            'AS9898',
            'AKJZDM',
            'ANHZ49',
            'ATH5IS',
            'A24XWB',
            'A4HMFM',
            'APRMQO',
            'A5Q5EX',
            'AFADHK',
            'ANQCE7',
            'AY0KSL',
            'A2PS8Q',
            'A4TRSD',
            'ADAHJF',
            'AQJ11S',
            'AXSLL9',
            'A11X2Y',
            'AKD52V',
            'A869ZU',
            'A1SWDB',
            'A3P9LD',
            'A4O0KI',
            'A7P1EC',
            'ACCHI4',
            'AZZU1X',
            'A5756Y',
            'ABC0BZ',
            'AJIKGJ',
            'AND5MS',
            'ATNCMH',
            'AY15EQ',
            'AYITZ9',
            'AFWKI2',
            'AHQUAC',
            'ALD9NB',
            'AWJN0H',
            'AE3YVD',
            'AAAT72',
            'AAO3TK',
            'ACV6MK',
            'AMR1J3',
            'ATPF3L',
            'AYNZ8S',
            'AQVWKZ',
            'ATKVN3',
            'AK3P45',
            'AFKIJ5',
            'A8UDOR',
            'AAT9H9',
            'AMMFWG',
            'AEIMUG',
            'AZBO4L',
            'A5EY2H',
            'ACJVN2',
            'ACMBJ8',
            'ACNHY8',
            'AGTKN9',
            'AHTMI3',
            'AIDHJ7',
            'AJGNE2',
            'AJLIY7',
            'AMIGK4',
            'ATKCI9',
            'AUGMD8',
            'AXJGK6',
            'AYBKF2',
            'AYNOE2',
            'ANBZOM',
            'AJ7MMZ',
            'A1I93P',
            'A7PJW3',
            'ABG3RF',
            'AGFT0P',
            'AIOYPC',
            'AKXSO6',
            'AQOTSP',
            'AB8S4K',
            'AI4IKN',
            'AE0C36',
            'A6JKLK',
            'A76R36',
            'AUGPON',
            'AK3IKS',
            'AL4T7N',
            'AK63Q3',
            'ANV6GJ',
            'AY7H0T',
            'AJS8M1',
            'A7XM74',
            'AO4IYB',
            'ARKVQ4',
            'AS62GI',
            'AT8QA0',
            'AZVWEI',
            'AN5RRD',
            'A8PRPT',
            'AE7WZ1',
            'AKU6T9',
            'ANRTI5',
            'AS1A98',
            'AF8EKF',
            'AH6DXY',
            'AL395W',
            'AXA2KL',
            'ABQJ2G',
            'AC4EFC',
            'ADSK9U',
            'AW8P82',
            'ABOTN6',
            'AEKLM1',
            'AFORPX',
            'AFUNR3',
            'AKFBD2',
            'AKFNE9',
            'AKFVE2',
            'AMQDJ7',
            'ANFLU6',
            'ANGLBX',
            'ANYGD4',
            'AQMDG1',
            'ARCIM2',
            'ARGIF1',
            'ATKMQ5',
            'AUBEI0',
            'AUHVX5',
            'AUSJC5',
            'AUVGD7',
            'AYBLD2',
            'AKOWQP',
            'AZ0LO2',
            'A2DXJ9',
            'AV38A5',
            'ACCILY',
            'ANJZWR',
            'AESFM8',
            'A5FRXL',
            'A8RH3P',
            'AD52R2',
            'AHFV1S',
            'AVKPPY',
            'A3XZ7Q',
            'AD4TH1',
            'AI5YOI',
            'AKXSO8',
            'A6B16P',
            'AKY3GR',
            'AHQNGF',
            'AOZCTH',
            'AQEBWB',
            'AQWZPV',
            'AUVERP',
            'A272G5',
            'AIXK54',
            'AL6LCB',
            'A3DYSD',
            'A4UXC5',
            'A4Z8AK',
            'AALNKN',
            'ABQRQO',
            'ADKDZ0',
            'ADT2TD',
            'ALA7IV',
            'AVZY5C',
            'AYZUL5',
            'AL56JO',
            'A211BM',
            'A50XZ4',
            'AHQF63',
            'AI4CNF',
            'AKPHHI',
            'AMOINS',
            'ANED6G',
            'ARGAUW',
            'AUL8P1',
            'AKSMNL',
            'AMNXCY',
            'A3X3GY',
            'ALM6YP',
            'APL885',
            'ARETNW',
            'ARETVE',
            'AWRETD',
            'AF4TO8',
            'AN4TO2',
            'A6KMTM',
            'A86FM7',
            'ATTVMY',
            'ANL3VZ',
            'A5YF6Z',
            'ACAEOH',
            'AHQJMU',
            'AM8UI1',
            'AMBEQW',
            'ANNK0N',
            'AR8D7C',
            'ADMNOG',
            'ADUKQJ',
            'AHETXZ',
            'AHJKRO',
            'AHJKRV',
            'AJLMY9',
            'AJLYM9',
            'AJLYMI',
            'AKENY0',
            'AKSMNZ',
            'AKSNY3',
            'ALZVNT',
            'ALZVNZ',
            'AM8PQO',
            'AMNSF9',
            'AMRDHK',
            'ANMAWY',
            'APOTGF',
            'AQDRH7',
            'AQRDHH',
            'AQRDHL',
            'ARETMN',
            'ARETV1',
            'ARETVH',
            'ARETVL',
            'ARMVWH',
            'ARTZY9',
            'ASNMJ4',
            'ASNMJ6',
            'ASNMJV',
            'ASRVWI',
            'ATVNYV',
            'AVYSTP',
            'AVYSTS',
            'AWRETK',
            'AZXQRE',
            'A3891H',
            'ARO6PN',
            'AB7TO7',
            'AE7TO6',
            'AE9TO9',
            'AF2TO0',
            'AG1TO2',
            'AH1TO4',
            'AK1TO0',
            'AL1TO2',
            'AL3TO2',
            'AM8TO2',
            'AM8TO5',
            'AN2TO5',
            'AN4TO9',
            'AN9TO5',
            'AO2TO2',
            'AQ0TO0',
            'AQ7TO4',
            'AQ7TO5',
            'AR7TO5',
            'AV9TO1',
            'AW0TO0',
            'AW3TO5',
            'AY2TO1',
            'AAU08E',
            'AATVL5',
            'AH6ZG2',
            'A6HFGU',
            'AWARE2',
            'AKHDQV',
            'AKZCYI',
            'AMPBQ6',
            'AQ8EM4',
            'AQUCIX',
            'AYBB65',
            'AKOEWS',
            'AT9YSL',
            'AQ0EVI',
            'AZR23T',
            'AFHQ4A',
            'AHHCL9',
            'AF2MS8',
            'AF4KZ0',
            'A2RPQP',
            'A6L0ND',
            'A6R8M7',
            'AA6WLB',
            'AAHB41',
            'AGG5JM',
            'AR3R5H',
            'AY3XAB',
            'AZYUWJ',
            'A4MEX6',
            'ADN4KP',
            'A5D210',
            'AE01DX',
            'AOHBIM',
            'A6TZN9',
            'ADC92N',
            'AL8ENO',
            'AGXIHI',
            'AR5GJX',
            'AUY2PO',
            'A3WV9E',
            'AW3471',
            'A91KDJ',
            'ARR12Q',
            'ARR22Q',
            'AYCLVB',
            'AYJOOQ',
            'A5B1OG',
            'A6YFER',
            'ADUKQF',
            'AEI6Z2',
            'AEOQXJ',
            'AFTYJ2',
            'AHETX1',
            'AHETXK',
            'AKNMY1',
            'AKNMYA',
            'AKNMYN',
            'AKSNMJ',
            'AKSNMS',
            'ALJYNF',
            'ALMD4U',
            'AMNSF0',
            'AMNXCN',
            'ANMJPR',
            'ANRKJ5',
            'ANRKJH',
            'AOQANR',
            'AQ2N4Z',
            'AQDRHF',
            'AQDRHR',
            'AQRDH9',
            'AROTG7',
            'AROTGM',
            'AROTGV',
            'AROTGW',
            'ARTZY8',
            'ARTZYY',
            'ASNMGF',
            'ASNMJD',
            'ASNMJL',
            'ATLKM0',
            'AVEWTG',
            'AVEWTL',
            'A12345',
            'ADDG28',
            'AKDJX7',
            'A2CL4L',
            'AGQJSH',
            'A66Q8L',
            'ASLYMB',
            'AONF6G',
            'AC9TO2',
            'AG5TO4',
            'AG9TO9',
            'AH1TO6',
            'AH5TO0',
            'AI1TO5',
            'AI8TO4',
            'AJ2TO1',
            'AJ2TO4',
            'AJ4TO0',
            'AJ6TO5',
            'AK0TO4',
            'AK7TO8',
            'AK8TO1',
            'AL4TO7',
            'AN1TO3',
            'AO0TO4',
            'AO8TO6',
            'AO9TO1',
            'AO9TO2',
            'AO9TO4',
            'AP4TO5',
            'AP8TO5',
            'AQ0TO1',
            'AQ0TO7',
            'AQ5TO7',
            'AV2TO2',
            'AV5TO0',
            'AV9TO8',
            'AW6TO3',
            'AY1TO8',
            'AC76RF',
            'AGEMJJ',
            'AMITA1',
            'AMOTA1',
            'AWARE8',
            'A38P4F',
            'AR2OMO',
            'A6LFIP',
            'A9G4AO',
            'ATBIPT',
            'A4AMZ1',
            'A8P41B',
            'A27646',
            'A2QNSA',
            'A7WVR8',
            'AK030E',
            'AG1CNU',
            'ACK3M2',
            'AF0MS9',
            'AF0XZ5',
            'AF89KM',
            'A69K0O',
            'ACRHOK',
            'A0OSAG',
            'A1FP52',
            'A3OVC8',
            'A4F06G',
            'A6M7AS',
            'A70693',
            'ACUWQD',
            'AF0NJI',
            'AFPX03',
            'AGXWVL',
            'AIFCJH',
            'ARDT0N',
            'ATD0A0',
            'ATS2K0',
            'AU6DA3',
            'AUP63U',
            'AV6YCS',
            'AYB8GF'
        );

//        dd($codesList6Digit);
//        $codesList8Digit = array('8EVFYYCF',
//            'BHW9B5RP',
//            '33M4YMC9',
//            'CH4RYZ4E',
//            'WFCHH5WN',
//            '9VYM93NR',
//            '8N8CU8E9',
//            'NPPAEHH9',
//            'E3NMP3M3',
//            'HEJ9NF4R',
//            '3737EET3',
//            '3FCJPJHC',
//            'FRF2FMRT',
//            'M5EBFFPP',
//            'NETPMNMH',
//            'RJY3NYWP',
//            'RNMFEY9S',
//            'RRVCT9Y9',
//            'Y3WRR3TC',
//            '2W9Y3FW9',
//            '99V3NF93',
//            'E3Z9FJHF',
//            'HFTTTJ93',
//            'ZTEZZFFJ',
//            'TFJPTJ9E');
//
//        $codesList10Digit = array('2P3NFJ32YC',
//            'C3TWPYCRYN',
//            'AHTFP7TTHN',
//            'JFVRHC7M9D',
//            'A123456789');
//
//        $allCodes = array_merge($codesList6Digit, array_merge($codesList8Digit, $codesList10Digit));

        $redemptionCodesDuplicatesWithParticipations = RedemptionCode::whereIn('code', $codesList6Digit)
            ->where('reusable', '=', 0)
            ->with('participation')
            ->get();


        if($redemptionCodesDuplicatesWithParticipations->groupBy('code')->count() <= 0){
            dd("No duplicate codes found");
        }

        $participationsThatRelateWithDuplicateCodes = [];

        // Remove codes that are resusable or not related with participation
        foreach($redemptionCodesDuplicatesWithParticipations as $redemptionCodesDuplicatesWithParticipation){

            if($redemptionCodesDuplicatesWithParticipation->participation == null){
                $jsonCode = new JsonCode();
                $jsonCode->redemption_id = $redemptionCodesDuplicatesWithParticipation->id;
                $jsonCode->code = $redemptionCodesDuplicatesWithParticipation->code;
                $duplicateCodesNotUsedTwice->add($jsonCode);
            }
            else {
                $participationsThatRelateWithDuplicateCode = $redemptionCodesDuplicatesWithParticipation->participation->id;

                if (!in_array($participationsThatRelateWithDuplicateCode, $participationsThatRelateWithDuplicateCodes)) {
                    $participationsThatRelateWithDuplicateCodes[] = $participationsThatRelateWithDuplicateCode;
                }
            }

        }

        $jsonUsers =  new \Illuminate\Support\Collection();

        foreach($redemptionCodesDuplicatesWithParticipations as $redemptionCodesDuplicatesWithParticipation){

            if($redemptionCodesDuplicatesWithParticipation->participation == null)
                continue;

            $user = $redemptionCodesDuplicatesWithParticipation->participation->user;

            if($user == null || count($emailFilter) != 0 && !in_array($user->email, $emailFilter))
                continue;

            if(!$jsonUsers->contains('id', $user->id)){

                $participationsWithDuplicateCodes = $user->participations()->with('redemptionCode')->whereIn('id', $participationsThatRelateWithDuplicateCodes)->get();

                $user->participations = $participationsWithDuplicateCodes;

                $jsonUser = new JsonUser();
                $jsonUser->id = $user->id;
                $jsonUser->email = $user->email;
                $jsonUser->currentPoints = $user->llSubscriptions[0]->points;
                $jsonUser->rawParticipations = $user->participations;
                $jsonUser->actionsToBeTaken = new \Illuminate\Support\Collection();
                $jsonUser->actionsTaken = new \Illuminate\Support\Collection();
                $jsonUser->validCodeAndParticipations = new \Illuminate\Support\Collection();


                foreach($jsonUser->rawParticipations as $rawParticipation){

                    $isCreatedFirstTime = false;

                    if(!isset($jsonUser->duplicateCodesAndParticipations[$rawParticipation->redemptionCode->code])){
                        $jsonUser->duplicateCodesAndParticipations[$rawParticipation->redemptionCode->code] = [];
                        $jsonUser->duplicateCodesAndParticipations[$rawParticipation->redemptionCode->code]['participations'] = new \Illuminate\Support\Collection();
                        $jsonUser->duplicateCodesAndParticipations[$rawParticipation->redemptionCode->code]['pointsToBeRemoved'] = 0;
                        $isCreatedFirstTime = true;
                    }

                    $jsonUser->duplicateCodesAndParticipations[$rawParticipation->redemptionCode->code]['participations']->add($rawParticipation);


                    $pointsToRemove = 0;
                    switch(strlen($rawParticipation->redemptionCode->code)){
                        case 10:
                        case 6:
                            $pointsToRemove += 40;
                            break;
                        case 8:
                            $pointsToRemove += 20;
                            break;
                        default:
                            break;
                    }

                    if(!$isCreatedFirstTime) {
                        $jsonUser->duplicateCodesAndParticipations[$rawParticipation->redemptionCode->code]['pointsToBeRemoved'] += $pointsToRemove;
                        $jsonUser->pointsToBeRemovedInTotal += $pointsToRemove;
                    }


                }

                if(count($emailsThatShouldNotRemoveCodes) > 0 && in_array($jsonUser->email, $emailsThatShouldNotRemoveCodes)){
                    $jsonUser->actionsToBeTaken->add("User should not be edited");
                    $jsonUser->shouldTakeAnyActions = false;
                    $jsonUser->canRemoveUsersPoints = false;
                }
                else if(in_array($jsonUser->email, $emailsThatShouldDirectlySetForZeroPoints)){
                    $jsonUser->actionsToBeTaken->add("User should have 0 zero points directly.");
                    $jsonUser->shouldTakeAnyActions = true;
                    $jsonUser->canRemoveUsersPoints = true;
                    $jsonUser->setUserToZeroPoints = true;

                }
                else {
                    foreach ($jsonUser->duplicateCodesAndParticipations as $code => $info) {

                        if ($info["pointsToBeRemoved"] <= 0) {
                            $jsonUser->totalValidParticipations++;
                            $jsonUser->validCodeAndParticipations->add($code);
                            $jsonUser->actionsToBeTaken->add("Skipping code " . $code . " because it only has 1 participation from user");
                        } else {
                            $jsonUser->totalDuplicateParticipations++;
                            $jsonUser->actionsToBeTaken->add("Remove " . $info["pointsToBeRemoved"] . " points for code " . $code .
                                " based on participations " .
                                $info["participations"]->pluck('id')->implode(',') .
                                ' (Points calculated by: participations(' . $info["participations"]->count() . ' - 1 = ' . ($info["participations"]->count() - 1) . ') * points(' . ($info["pointsToBeRemoved"] / ($info["participations"]->count() - 1 == 0 ? 1 : $info["participations"]->count() - 1)) . ')');
                        }
                    }

                    if($jsonUser->pointsToBeRemovedInTotal != 0){
                        $jsonUser->shouldTakeAnyActions = true;
                    }

                    if($jsonUser->currentPoints >= $jsonUser->pointsToBeRemovedInTotal && $jsonUser->currentPoints != 0 && $jsonUser->pointsToBeRemovedInTotal != 0)
                        $jsonUser->canRemoveUsersPoints = true;

                }



                $jsonUsers->add($jsonUser);

            }

        }

        if(!$shouldExportAsCsv)
            dd($jsonUsers);

        // $this->removePointsFrom($jsonUsers);

        // $this->addInDebtPointsToUsers($jsonUsers);
        // $this->exportUsers($jsonUsers);


    }
    
    
     function addInDebtPointsToUsers($jsonUsers){

        $actionForAddingDebt = \DB::table('ll_actions')->where('slug', 'indebtadd')->first();

        if($actionForAddingDebt == null || !$actionForAddingDebt)
            dd("Missing action indebtadd");



        foreach($jsonUsers as $user) {

            if(!$user->shouldTakeAnyActions){
                $user->actionsTaken->add("Did not modified user because we shouldnt take any actions");
                continue;
            }

            if($user->canRemoveUsersPoints || $user->setUserToZeroPoints){
                $user->actionsTaken->add("Did not modified user, user not contained in list for indebt mechanism.");
                continue;
            }

            try {

                $subscription = \laravel\loyalty\Entities\Subscription::where('user_id', $user->id)->first();

                if($subscription == null || !$subscription){
                    $user->actionsTaken->add("Failed because subscription not found");
                    continue;
                }

                $alreadyAddedDebtPoints = $subscription->transactions()->where('action_id', $actionForAddingDebt->id)->exists();

                if($alreadyAddedDebtPoints){
                    $user->actionsTaken->add("Already added points as debt");
                    continue;
                }


                $subscription->update(['indebtPoints' => $user->pointsToBeRemovedInTotal]);

                $description = "Added " . $user->pointsToBeRemovedInTotal . " points as debt";

                $transaction = $subscription->transactions()->create([
                    'action_id' => $actionForAddingDebt->id,
                ]);


                $transaction->description()->create([
                    'description' => $description
                ]);

                $user->actionsTaken->add($description);
            }
            catch(\Exception $e){
                $user->actionsTaken->add("Failed because of " . $e->getMessage());

            }

        }

    }


    function removePointsFrom($jsonUsers){

        $actionForRemoval = \DB::table('ll_actions')->where('slug', 'autoremove')->first();

        if($actionForRemoval == null || !$actionForRemoval)
            dd("Missing action autoremove");



        foreach($jsonUsers as $user){

            if(!$user->shouldTakeAnyActions){
                $user->actionsTaken->add("Did not modified user because we shouldnt take any actions");
                continue;
            }
            if(!$user->canRemoveUsersPoints){
                $user->actionsTaken->add("Did not modified user, will be handled by indebt mechanism");
                continue;
            }
            if($user->currentPoints == 0) {
                $user->actionsTaken->add("Did not modified user because current points are 0");
                continue;
            }

            if($user->currentPoints != 0 && !$user->setUserToZeroPoints && $user->currentPoints < $user->pointsToBeRemovedInTotal){
                $user->actionsTaken->add("Did not modified user because current points(".$user->currentPoints.") are lower than points to be removed(".$user->pointsToBeRemovedInTotal.")");
                continue;
            }

            if( $user->pointsToBeRemovedInTotal == 0 && !$user->setUserToZeroPoints){
                $user->actionsTaken->add("Did not modified user because points to be removed is 0");
                continue;
            }

            try {

                $subscription = \laravel\loyalty\Entities\Subscription::where('user_id', $user->id)->first();

                if($subscription == null || !$subscription){
                    $user->actionsTaken->add("Failed because subscription not found");
                    continue;
                }

                $alreadyRemovedPoints = $subscription->transactions()->where('action_id', $actionForRemoval->id)->exists();

                if($alreadyRemovedPoints){
                    $user->actionsTaken->add("Already removed points");
                    continue;
                }

                $pointsToSet = $user->currentPoints - $user->pointsToBeRemovedInTotal;

                $description = 'Removed ' . $user->pointsToBeRemovedInTotal . ' points for participations';

                if($user->setUserToZeroPoints) {
                    $pointsToSet = 0;
                    $description = 'Directly set 0 points';
                }

                $subscription->update(['points' => $pointsToSet]);

                $transaction = $subscription->transactions()->create([
                    'action_id' => $actionForRemoval->id,
                ]);


                $transaction->description()->create([
                    'description' => $description
                ]);


                $user->actionsTaken->add($description);
            }
            catch(\Exception $e){
                $user->actionsTaken->add("Failed because of " . $e->getMessage());

            }

        }

    }

    function exportUsers($jsonUsers){
        /* ECHO CSV */
        ob_start();

        $cellvalue =
            '"Email",'.
            '"Total Participations",'.
            '"Total Duplicates Codes Used",'.
            '"Valid Duplicate Codes Used",'.
            '"Current Points",'.
            '"Points To Be Removed",'.
            '"Can Points be removed directly",'.
            '"Should we take any actions",'.
            '"Actions",'.
            '"Actions Taken",'.

            "\n";
        echo $cellvalue;

        foreach($jsonUsers as $user){

            $cellvalue =
                '"'.$user->email.'",'.
                '"'.count($user->rawParticipations).'",'.
                '"'.count($user->duplicateCodesAndParticipations).'",'.
                '"'.$user->totalDuplicateParticipations.'",'.
                '"'.$user->currentPoints.'",'.
                '"'.$user->pointsToBeRemovedInTotal.'",'.
                '"'.($user->canRemoveUsersPoints ? "YES" : "NO").'",'.
                '"'.($user->shouldTakeAnyActions ? "YES" : "NO").'",'.
                '"'.$user->actionsToBeTaken->implode('').'",'.
                '"'.$user->actionsTaken->implode('').'",'.

                "\n";
            echo $cellvalue;

        }

        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename=test.csv");
        header("Content-Transfer-Encoding: binary");
        ob_end_flush();

    }

    function exportDuplicateCodesToBeRemoved($duplicateCodesNotUsedTwice){
        /* ECHO CSV */
        ob_start();

        $cellvalue =
            '"Code",'.
            '"Redemption ID",'.
            "\n";
        echo $cellvalue;

        foreach($duplicateCodesNotUsedTwice as $jsonCode){

            $cellvalue =
                '"'.$jsonCode->code.'",'.
                '"'.$jsonCode->redemption_id.'",'.
                "\n";
            echo $cellvalue;

        }

        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename=test.csv");
        header("Content-Transfer-Encoding: binary");
        ob_end_flush();

    }

}

class JsonUser{

    public $id;
    public $rawParticipations = [];
    public $duplicateCodesAndParticipations = [];
    public $totalDuplicateParticipations = 0;
    public $totalValidParticipations = 0;
    public $email;
    public $currentPoints = 0;
    public $pointsToBeRemovedInTotal = 0;
    public $canRemoveUsersPoints = false;
    public $shouldTakeAnyActions = false;
    public $removedPoints = false;
    public $setUserToZeroPoints = false;

}

class JsonCode{
    public $redemption_id;
    public $code;
}

