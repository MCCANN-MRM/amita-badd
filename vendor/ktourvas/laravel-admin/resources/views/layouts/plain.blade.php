<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MRM//MCCANN - Manager</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="/assets/vendor/ladmin/css/vendor.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendor/ladmin/css/app.css">
</head>

<body>

@yield('content')

</body>
</html>
