<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        {!! config('laravel-admin.front.meta.title') !!}
    </title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="/assets/vendor/ladmin/css/vendor.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendor/ladmin/css/app.css">
</head>

<body>

<div class="mrm-admin">

    <div class="layout">

        <div class="menu-icon" @click="toggleMenu">
            <i class="fas fa-bars"></i>
        </div>

        <div class="header">
            @include('mma::partials.navbar')
        </div>

        <div class="sidenav" v-bind:class="{ 'active': menu.on }">

            <aside class="menu">

                <div class="sidenav__close-icon" @click="toggleMenu">
                    <i class="fas fa-times"></i>
                </div>

                @include('mma::partials.sidebar')

            </aside>

        </div>

        <div class="main">

            <div class="columns">

                <div class="column">

                    @yield('content')

                </div>

            </div>

        </div>

        <div class="footer">
            @include('mma::partials.footer')
        </div>

    </div>

    @include('mma::partials.modals')

</div>

<script src="{{ mix('/js/manifest.js', 'assets/vendor/ladmin') }}"></script>
<script src="{{ mix('/js/vendor.js', 'assets/vendor/ladmin') }}"></script>
<script src="{{ mix('/js/app.js', 'assets/vendor/ladmin') }}"></script>

@stack('scripts')

</body>
</html>
