@extends('mma::layouts.main')

@section('content')

    {{-- render the tiles --}}
    <div class="container">

        <div class="tile is-ancestor">
            @foreach($tiles as $tile)

                <div class="tile is-parent">

                    <article class="tile is-child box">
                        <a href="{{ config('laravel-admin.root_url') }}/{{ $tile['url'] }}">
                            <p class="title">
                                {{ $tile['number'] }}
                            </p>
                            <p class="subtitle">
                                {{ $tile['title'] }}
                            </p>
                        </a>
                    </article>

                </div>
            @endforeach
        </div>

        {{--{{ dd($graphs) }}--}}

        <div class="tile is-ancestor is-centered">

            <div class="tile is-parent is-12 is-vertical ">
            @foreach($graphs as $graph)

                    <article class="tile is-12 is-child box">

                        <line-graph src="{{ $graph['graph']['source'] }}"
                                    :labels="{{ json_encode($graph['graph']['data']['x']) }}"
                                    :data="{{ json_encode($graph['graph']['data']['y']) }}"
                                    :render="{'label':'# of participations'}"
                                    title="{{ $graph['title'] }}"
                        >
                        </line-graph>

                        @foreach($graph['actions'] as $action)

                            <f-post inline-template action="{{ $action['endpoint'] }}">

                                <div class="field has-addons has-addons-centered">
{{--                                    <p class="control">--}}
{{--                                        <span class="select">--}}
{{--                                            <select v-model="added.draw">--}}
{{--                                                @foreach($promo->adhocDraws() as $key => $draw)--}}
{{--                                                    <option value="{{ $key }}">{{ $draw['title'] }}</option>--}}
{{--                                                @endforeach--}}
{{--                                            </select>--}}
{{--                                        </span>--}}
{{--                                    </p>--}}
                                    <p class="control">
                                        <button class="button is-success" @click="onSubmit" :disabled="sending">
                                            <span class="icon is-small" v-if="sending">
                                                <i class="fas fa-spinner fa-spin"></i>
                                            </span>
                                            <span>{{ $action['label'] }}</span>
                                            <span class="icon is-small">
                                                <i class="fas fa-random"></i>
                                            </span>
                                        </button>
                                    </p>
                                </div>

                            </f-post>

                        @endforeach

                    </article>

            @endforeach
            </div>
        </div>

    </div>

@endsection

@section('js')

@stop