@extends('mma::layouts.plain')

@section('content')

    <section class="hero is-fullheight">

        <div class="hero-body is-centered">

            <div class="container">

                <div class="columns is-centered">

                    <div class="column is-5-widescreen has-text-centered">

                        <img src="{{ config('laravel-admin.front.logo.image') }}"
                             alt="{{ config('laravel-admin.front.logo.alt') }}" style="max-width: 300px">

                    </div>

                </div>

                <div class="columns is-centered">

                    <div class="box column is-5-widescreen">

                        <form method="POST" action="{{ route('login') }}">

                            @csrf

                            <div class="field">
                                <label for="" class="label">Email</label>
                                <div class="control has-icons-left">
                                    <input id="email" type="email" class="input{{ $errors->has('email') ? ' is-danger' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                    <span class="icon is-small is-left">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                </div>

                                @if ($errors->has('email'))
                                    <p class="help is-danger">
                                        {{ $errors->first('email') }}
                                    </p>
                                @endif

                            </div>

                            <div class="field">
                                <label for="" class="label">Password</label>
                                <div class="control has-icons-left">
                                    <input id="password" type="password" class="input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                    <span class="icon is-small is-left">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </div>
                                @if ($errors->has('password'))
                                    <p class="help is-danger">
                                        {{ $errors->first('password') }}
                                    </p>
                                @endif
                            </div>

                            <div class="field">
                                <label for="" class="checkbox">
                                    <input class="" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    Remember me
                                </label>
                            </div>

                            <div class="field">
                                <button class="button is-success is-fullwidth">
                                    Login
                                </button>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>
    </section>

@endsection
