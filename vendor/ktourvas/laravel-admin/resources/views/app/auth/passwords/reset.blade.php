@extends('mma::layouts.plain')

@section('content')

    <section class="hero is-fullheight">

        <div class="hero-body is-centered">

            <div class="container">

                <div class="columns is-centered">

                    <div class="column is-5-widescreen">
                        <img src="{{ config('laravel-admin.front.logo.image') }}"
                             alt="{{ config('laravel-admin.front.logo.alt') }}" style="max-width: 300px">
                    </div>

                </div>

                <div class="columns is-centered">

                    <div class="box column is-5-widescreen">

                        {{ $errors }}

                        <form method="POST" action="{{ route('password.update') }}">

                            @csrf

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="field">
                                <label for="" class="label">Email</label>
                                <div class="control has-icons-left">
                                    <input id="email" type="email" class="input{{ $errors->has('email') ? ' is-danger' : '' }}" name="email" value="{{ old('email') }}" required>
                                    <span class="icon is-small is-left">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                </div>

                                @if ($errors->has('email'))
                                    <p class="help is-danger">
                                        {{ $errors->first('email') }}
                                    </p>
                                @endif

                            </div>

                            <div class="field">
                                <label for="" class="label">Password</label>
                                <div class="control has-icons-left">
                                    <input id="password" type="password" class="input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                    <span class="icon is-small is-left">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </div>
                                @if ($errors->has('password'))
                                    <p class="help is-danger">
                                        {{ $errors->first('password') }}
                                    </p>
                                @endif
                            </div>

                            <div class="field">
                                <label for="password-confirm" class="label">Confirm Password</label>
                                <div class="control has-icons-left">
                                    <input id="password-confirm" type="password" class="input" name="password_confirmation" required>
                                    <span class="icon is-small is-left">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="field">
                                <button class="button is-primary is-fullwidth">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>
    </section>

@endsection
