@extends('mma::layouts.plain')

@section('content')

    <section class="hero is-fullheight">

        <div class="hero-body is-centered">

            <div class="container">

                <div class="columns is-centered">

                    <div class="column is-5-widescreen">
                        <img src="{{ config('laravel-admin.front.logo.image') }}"
                             alt="{{ config('laravel-admin.front.logo.alt') }}" style="max-width: 300px">
                    </div>

                </div>

                <div class="columns is-centered">

                    <div class="box column is-5-widescreen">

                        @if (session('status'))
                            <div class="notification is-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">

                            @csrf

                            <div class="field">
                                <label for="" class="label">Email</label>
                                <div class="control has-icons-left">
                                    <input id="email" type="email"
                                           class="input{{ $errors->has('email') ? ' is-danger' : '' }}"
                                           name="email" value="{{ old('email') }}" required autofocus>
                                    <span class="icon is-small is-left">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                </div>
                                @if ($errors->has('email'))
                                    <p class="help is-danger">
                                        {{ $errors->first('email') }}
                                    </p>
                                @endif
                            </div>

                            <div class="field">
                                <button class="button is-fullwidth is-info">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection
