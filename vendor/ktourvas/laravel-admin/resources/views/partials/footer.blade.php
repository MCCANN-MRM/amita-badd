<nav class="navbar">

    <div class="navbar-brand">
        <a class="navbar-item">
            <img src="{{ config('laravel-admin.front.logo.image') }}"
                 alt="{{ config('laravel-admin.front.logo.alt') }}" class="logo" style="max-width: 100px;">
        </a>
    </div>

    <div id="navbarMenuHeroB" class="navbar-menu">
        <div class="navbar-end">
            <p class="navbar-item is-size-7">
                {!! config('laravel-admin.front.footer.right') !!}
            </p>
        </div>
    </div>

</nav>