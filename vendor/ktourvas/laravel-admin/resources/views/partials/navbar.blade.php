<nav class="navbar">
    <div class="navbar-brand">
        <a class="navbar-item">
            <img src="{{ config('laravel-admin.front.logo.image') }}"
                 alt="{{ config('laravel-admin.front.logo.alt') }}" class="logo">
        </a>
        <span class="navbar-burger burger" data-target="navbarMenuHeroB">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
    </div>
    <div id="navbarMenuHeroB" class="navbar-menu">
        <div class="navbar-end">
            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link">
                    {{ Auth::user()->name }}
                </a>
                <div class="navbar-dropdown">
                    <a class="navbar-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                    <form id="logout-form" action="/logout" method="POST" style="display: none;">
{{--                        @csrf--}}
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</nav>