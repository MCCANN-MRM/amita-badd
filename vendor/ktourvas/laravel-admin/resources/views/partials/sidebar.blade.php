<p class="menu-label">
    Γενικά
</p>

<ul class="menu-list">
    <li><a href="/{{ config('laravel-admin.root_url') }}">Dashboard</a></li>
</ul>

@if(!empty(config('laravel-admin.sidebar_includes')))

    @foreach(config('laravel-admin.sidebar_includes') as $block)

        @if(!empty($block['head']))
            <p class="menu-label">
                {{ $block['head']['label'] }}
            </p>
        @endif

        @if(!empty($block['children']))
            <ul class="menu-list">
            @foreach($block['children'] as $child)
                <li>
                    <a href="/{{ config('laravel-admin.root_url') }}{{ $child['url'] }}"
                            {!! \Request::is($child['url'].'/*') ? ' class="is-active"' : '' !!}>
                        {{ $child['label'] }}
                    </a>
                </li>
            @endforeach
            </ul>
        @endif

    @endforeach

@endif

<p class="menu-label">
    Logs
</p>

<ul class="menu-list">
    <li>
        <a href="/{{ config('laravel-admin.root_url') }}/logs/error" target="_blank">
            Error Log
        </a>
    </li>
</ul>

@stack('sidebarmenu')
