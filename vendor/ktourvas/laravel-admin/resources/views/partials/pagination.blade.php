@if ($paginator->hasPages())
    <nav class="pagination is-centered" role="navigation" aria-label="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            {{--<li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">--}}
                {{--<span class="page-link" aria-hidden="true">&lsaquo;</span>--}}
            {{--</li>--}}
        @else
            {{--<li class="page-item">--}}
                {{--<a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>--}}
            {{--</li>--}}
        @endif
        <ul class="pagination-list">
        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li>
                            <a class="pagination-link is-current" aria-label="Page 46" aria-current="page">{{ $page }}</a>
                        </li>
                        {{--<li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>--}}
                    @else
                        <li>
                            <a class="pagination-link" aria-label="Goto page {{ $page }}" href="{{ $url }}">{{ $page }}</a>
                        </li>
                        {{--<li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>--}}
                    @endif
                @endforeach
            @endif
        @endforeach
        </ul>

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            {{--<li class="page-item">--}}
                {{--<a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>--}}
            {{--</li>--}}
        @else
            {{--<li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">--}}
                {{--<span class="page-link" aria-hidden="true">&rsaquo;</span>--}}
            {{--</li>--}}
        @endif
    </nav>
@endif


{{--<nav class="pagination" role="navigation" aria-label="pagination">--}}
    {{--<a class="pagination-previous">Previous</a>--}}
    {{--<a class="pagination-next">Next page</a>--}}
    {{--<ul class="pagination-list">--}}
        {{--<li>--}}
            {{--<a class="pagination-link" aria-label="Goto page 1">1</a>--}}
        {{--</li>--}}
        {{--<li>--}}
            {{--<span class="pagination-ellipsis">&hellip;</span>--}}
        {{--</li>--}}
        {{--<li>--}}
            {{--<a class="pagination-link" aria-label="Goto page 45">45</a>--}}
        {{--</li>--}}
        {{--<li>--}}
            {{--<a class="pagination-link is-current" aria-label="Page 46" aria-current="page">46</a>--}}
        {{--</li>--}}
        {{--<li>--}}
            {{--<a class="pagination-link" aria-label="Goto page 47">47</a>--}}
        {{--</li>--}}
        {{--<li>--}}
            {{--<span class="pagination-ellipsis">&hellip;</span>--}}
        {{--</li>--}}
        {{--<li>--}}
            {{--<a class="pagination-link" aria-label="Goto page 86">86</a>--}}
        {{--</li>--}}
    {{--</ul>--}}
{{--</nav>--}}
