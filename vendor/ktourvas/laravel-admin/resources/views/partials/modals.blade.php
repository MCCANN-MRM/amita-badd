<mma-dialog inline-template>

    <div class="modal" v-bind:class="{ 'is-active' : shown }">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title">@{{ headText }}</p>
                <button class="delete" aria-label="close" @click="close"></button>
            </header>
            <section class="modal-card-body">
                @{{ text }}
            </section>
            <footer class="modal-card-foot">
                <button class="button is-success" @click="confirm">Επιβεβαίωση</button>
                <button class="button" @click="close">Ακύρωση</button>
            </footer>
        </div>
    </div>

</mma-dialog>
