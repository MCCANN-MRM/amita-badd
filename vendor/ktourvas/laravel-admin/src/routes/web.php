<?php

$rootURL = config('laravel-admin.root_url');

Route::group([ 'middleware' =>

    array_merge(
        [
            'web'
        ],
        ((config('laravel-admin.iprestrict')) ? [ 'ipRestrict' ] : [])
    )

     ], function () use ($rootURL) {

    Route::group([ 'middleware' => [ 'web', 'LaravelAdmin' ] ], function () use ($rootURL) {

        Route::get($rootURL, 'laravel\admin\Http\Controllers\LaravelAdminController@index');

        Route::get($rootURL.'/logs/error', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    });

});