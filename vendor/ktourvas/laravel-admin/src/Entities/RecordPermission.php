<?php

namespace laravel\admin\Entities;

use Illuminate\Database\Eloquent\Model;

class RecordPermission extends Model
{
    protected $table = 'admin_record_permissions';

    public function set() {

        return $this->belongsTo('laravel\admin\Entities\PermissionSet', 'policy_id');

    }
}