<?php

namespace laravel\admin\Entities;

trait Adminable {

    public function isAdministrator() {
        return $this->userIs('admin');
    }

}