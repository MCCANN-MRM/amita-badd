<?php

namespace laravel\admin\Entities;

use Illuminate\Database\Eloquent\Model;

class PermissionsSet extends Model
{
    protected $table = 'admin_permissions';

    public function records() {

        return $this->hasMany('laravel\admin\Entities\RecordPermission', 'policy_id');

    }

}