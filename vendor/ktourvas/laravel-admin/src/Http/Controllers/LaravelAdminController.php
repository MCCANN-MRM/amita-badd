<?php

namespace laravel\admin\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LaravelAdminController extends Controller {

    public function __construct(Request $request){}

    public function index(Request $request)
    {
        $blocks = $this->dashboardBlocks();

        $tiles = $graphs = [];

        foreach ($blocks as $block) {

            $tiles = array_merge(

                $tiles,

                collect($block)->map(function($box) {
                    return $box;
                })->reject(function($box) {
                    return $box['type'] != 'number-tile';
                })->toArray()

            );

            $graphs = array_merge(

                $graphs,

                collect($block)->map(function($box) {
                    return $box;
                })->reject(function($box) {
                    return $box['type'] != 'graph';
                })->toArray()

            );

        }

        return view('mma::app.index', [
            'tiles' => $tiles,
            'graphs' => $graphs,
            'blocks' => $this->dashboardBlocks()
        ]);
    }

    public function login(Request $request)
    {
        return view('mma::public.login');
    }

    protected function dashboardBlocks() {
        $blocks = [];
        foreach(config('laravel-admin.dashboard.blocks') as $block) {
            $inv = new $block;
            $blocks[] = $inv();
        }
        return $blocks;
    }

}