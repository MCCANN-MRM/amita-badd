<?php

namespace laravel\admin\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle the incoming request.
     *
     * - In case of ip restrictions enforced to the administration routes,
     * the middleware can prompt guest users to login when a admin/* route
     * is requested, since apparently the request ip address is whitelisted.
     * If none of the above occurs, a 404 will be returned, avoiding to hint
     * that the route exists.
     *
     * - In case the user is authenticated and not an administrator a 404 is
     * returned avoiding to hint that the route exists.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::guest()) {
            if(config('laravel-admin.iprestrict')) {
                if(\Route::has('login')) {
                    return redirect(route('login'));
                }
            }
            abort(404);
        }

        if ( ! $request->user()->isAdministrator() ) {
            abort(404);
        }

        $this->sidebar($request);

        return $next($request);
    }

    private function sidebar($request) {

        $sidebar = !empty(config('laravel-admin.sidebar_includes'))  ? config('laravel-admin.sidebar_includes') : [];

        foreach( $sidebar as $key => $block ) {

            foreach(!empty($block['authorize']) ? $block['authorize'] : [] as $authorizationItem) {

                if( is_array( $authorizationItem ) ) {
                    if( ! $request->user()->can($authorizationItem[0], $authorizationItem[1]) ) {
                        unset( $sidebar[$key] );
                        break;
                    }
                } else {
                    if( ! $request->user()->can('viewAny', $authorizationItem) ) {
                        unset( $sidebar[$key] );
                        break;
                    }
                }

            }

        }

        config( [ 'laravel-admin.sidebar_includes' => $sidebar ] );

//        if( !empty( config('laravel-admin.sidebar_includes') ) ) {
//            $new = [  ];
//            foreach( config('laravel-admin.sidebar_includes') as $key => $block ) {
//                $view = [ true ];
//                foreach($block['authorize'] ?? [] as $model) {
//                    $view[] = $request->user()->can('viewAny', $model);
//                }
//                if( !empty($view) && !in_array(false, $view) ) {
//                    $new[$key] = $block;
//                }
//                config( [ 'laravel-admin.sidebar_includes' => $new ] );
//            }
//        }

    }


}
