<?php

namespace laravel\admin;
use Illuminate\Support\ServiceProvider;

class LaravelAdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'mma');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        if (! $this->app->routesAreCached()) {
            require __DIR__.'/routes/web.php';
        }

        $this->publishes([
            __DIR__.'/../config/laravel-admin.php' => config_path('laravel-admin.php'),
        ]);

        $this->publishes([
            __DIR__.'/../resources/dist' => public_path('assets/vendor/ladmin'),
        ], 'public');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app['router']->aliasMiddleware('ipRestrict', \Laravel\extensions\Http\Middleware\ipRestrict::class);
        $this->app['router']->aliasMiddleware('LaravelAdmin', \laravel\admin\Http\Middleware\Admin::class);
    }
}
