<?php

namespace laravel\auth\journeys\Entities;

use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

trait HasCustomPasswordResetNotification
{
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $notification = new ResetPasswordNotification($token);

        $notification->toMailUsing( function () use ($token) {
            return (new MailMessage)
                ->subject(Lang::get('Reset Password Notification'))
                ->line(Lang::get('Παρακάτω το link για να κάνεις επαναφορά του κωδικού σου.'))
                ->action(Lang::get('Reset Password'), url(config('app.url').route('password.reset', [
                    'token' => $token,
// Sending the email address of the user over a url variable may be considered as a security risk.
// Uncomment the following in order to return to the normal laravel functionality
//                        'email' => $notifiable->getEmailForPasswordReset()

                    ], false)))
                ->line(Lang::get('Το link θα είναι ενεργό για 60 λεπτά.', ['count' => config('auth.passwords.'.config('auth.defaults.passwords').'.expire')]))
                ->line(Lang::get('Αν δεν ζήτησες την επαναφορά του κωδικού σου, δεν χρειάζεται να κάνεις κάτι.'));

        });

        $this->notify($notification);

    }

}