<?php
include "SimpleXLSXGen.php";
$sql = "SELECT\n"

    . "    u.id, u.email,td.description, t.action_id, t.created_at\n"

    . "FROM\n"

    . "    `ll_transactions` AS t\n"

    . "JOIN ll_transaction_descriptions AS td\n"

    . "ON\n"

    . "    t.id = td.transaction_id\n"

    . "JOIN ll_subscriptions AS s\n"

    . "ON\n"

    . "    s.id = t.subscription_id\n"

    . "JOIN users AS u\n"

    . "ON\n"

    . "    u.id = s.user_id\n"

    . "WHERE\n"

    . "    t.created_at >=(DATE(NOW()) - 1) AND(\n"

    . "        t.action_id = 3 OR t.action_id = 4\n"

    . "    )  \n"

    . "ORDER BY `t`.`created_at`  ASC";


$mysqli = mysqli_connect("localhost", "amitamot_l6x", "jsdh8530hHD(Y(#09su12893yfye2987y*F#T*(08(@", "amitamot_l6x");
//$mysqli = mysqli_connect("localhost", "root", "Dgtitt@m1974!", "amitamot_l6x");

// Check connection
if (!$mysqli) {
    die("Connection error" . $mysqli->connect_error);
}
if ($result = $mysqli->query($sql)) {
    $new_file = "public_html/app6l/deployment/public/reports/";
    $new_name = 'report_' . date("YmdHis") . '.xlsx';

    $new_file .= $new_name;
    $resultArray=$result->fetch_all();


    $xlsx = SimpleXLSXGen::fromArray( $resultArray );
    $xlsx->saveAs($new_file);
    /*$fp = fopen($new_file, 'x+');

    foreach ($result->fetch_all() as $fields) {
        fputcsv($fp, $fields);
    }

    fclose($fp);*/
    $response = "<p>This is an automated report from amitamotion.gr for "
        . date("d-m-Y")
        . "</p><a href='https://amitamotion.gr/reports/" . $new_name. "'>Download report</a>".display_data($result);;


    $to_email = 'tassos.ioannidis@mrm-mccann.gr';//, katerina.yiannouli@mrm-mccann.gr
    $subject = 'amitamotion.gr report ' . date("d-m-Y");
    $message = $response;
    // To send HTML mail, the Content-type header must be set
    $headers = 'MIME-Version: å1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Create email headers
    $headers .= 'From: report@amitamotion.gr' . "\r\n" .
        'Reply-To: ' . "report@amitamotion.gr" . "\r\n" .
        'X-Mailer: PHP/' . phpversion();


    mail($to_email, $subject, $message, $headers);

    $result->free_result();
}
$mysqli->close();



function display_data($data)
{
    $output = '<table>';
    foreach ($data as $key => $var) {
        $output .= '<tr>';
        foreach ($var as $k => $v) {
            if ($key === 0) {
                $output .= '<td><strong>' . $k . '</strong></td>';
            } else {
                if (strpos($v, "ΚΩΔΙΚΌΣ:") === true) {
                    $vval = str_replace("ΚΩΔΙΚΌΣ:", "", $v);
                } else {
                    $vval = $v;
                }
                $output .= '<td>' . $vval . '</td>';
            }
        }
        $output .= '</tr>';
    }
    $output .= '</table>';
    return $output;
}

