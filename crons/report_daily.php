<?php
include "SimpleXLSXGen.php";
$local = false;
//$local = true;
                                                    $query = "SELECT      u.id,     u.email,     INSERT(td.description, 1, 9, ''),       t.created_at FROM     `ll_transactions` AS t         JOIN     ll_transaction_descriptions AS td ON t.id = td.transaction_id         JOIN     ll_subscriptions AS s ON s.id = t.subscription_id         JOIN     users AS u ON u.id = s.user_id WHERE     t.created_at >= (DATE(NOW()) - 1)         AND (t.action_id = 3 OR t.action_id = 4) ORDER BY `u`.`id` ASC";

if (!$local) {
    $mysqli = mysqli_connect("localhost", "amitamot_l6x", "jsdh8530hHD(Y(#09su12893yfye2987y*F#T*(08(@", "amitamot_l6x");
} else {

    $mysqli = mysqli_connect("localhost", "root", "Dgtitt@m1974!", "amitamot_l6x");
}
// Check connection
if (!$mysqli) {

    die("Connection error" . $mysqli->connect_error);
}


if ($result = $mysqli->query($query, MYSQLI_USE_RESULT)) {
    $new_file = "public_html/app6l/deployment/public/reports/";
    $new_name = 'report_' . date("YmdHis") . '.xlsx';
    //var_dump($result);
    //display_data($result);
    $new_file .= $new_name;
    if (!$local) {



        $xlsx = SimpleXLSXGen::fromArray($result->fetch_all());
        $xlsx->saveAs($new_file);



    }
    $pretty_groups = make_groups($result);
    $suspects = [];
    $limitups = [];
    // var_dump($pretty_groups[0]);
    foreach ($pretty_groups as $group) {
        $current_user = $group["user"];
        $total_matches = check_user_codes($group["codes"]);
        if ($total_matches > 0) {
            $suspects[$current_user] = $total_matches;

        };
      // var_dump($group["codes"]);
        if (count($group["codes"]) > 9) {
            array_push($limitups, $current_user);
        }
    }
    send_report($new_name, $suspects, $limitups,$local);
    //var_dump($suspects);
    $result->free_result();
}
$mysqli->close();


function display_data($data)
{
    $output = '<table>';
    foreach ($data as $key => $var) {
        $output .= '<tr>';
        foreach ($var as $k => $v) {
            if ($key === 0) {
                $output .= '<td><strong>' . $k . '</strong></td>';
            } else {

                $output .= '<td>' . $v . '</td>';
            }
        }
        $output .= '</tr>';
    }
    $output .= '</table>';
    return $output;
}

function check_strings($string1, $string2)
{

    $codes_matched = false;

    if (strlen($string1) == strlen($string2)) {
        $str_arr1 = str_split($string1);
        $str_arr2 = str_split($string2);
        $matches = 0;
        $total_chars = strlen($string1);
        foreach ($str_arr1 as $idx => $val) {
            if ($str_arr1[$idx] == $str_arr2[$idx]) {
                $matches++;
            }
        }
        if ($matches == $total_chars - 1) {
            $codes_matched = true;
        }
        return $codes_matched;
    }
}

function check_user_codes($codes)
{
    $is_a_match = 0;
    $codes_num = count($codes);
    foreach ($codes as $idx => $code) {
        /*  if (has_next($codes)) {
              $is_a_match = check_strings(current($codes), next($codes));

          }*/
        for ($i = $idx + 1; $i < $codes_num; $i++) {

            if (check_strings($code, $codes[$i])) {
                $is_a_match++;
            }
        }


    }
    return $is_a_match;
}

function make_groups($all_groups)
{

    $group_id = null;
    $groups = [];
    $current_array = [];
    while ($row = $all_groups->fetch_array(MYSQLI_NUM)) {


        if ($row[0] != $group_id) {

            if ($group_id != null) {

                array_push($groups, $current_array);

            }

            $group_array = ["user" => $row[0], "codes" => []];
            $current_array = $group_array;


        }
        $group_id = $row[0];

        array_push($current_array['codes'], $row[2]);


    }
    return $groups;
}

function display_table_data($data)
{
    $output = '<table>';
    foreach ($data as $key => $var) {
        $output .= '<tr>';
        $output .= '<td>';
        $output .= $key;
        // $output .= '</td><td>' ;
        // $output .=  $var ;
        $output .= '</td></tr>';

    }
    $output .= '</table>';
    return $output;
}

function send_report($filename, $suspects_val, $limitups_val,$is_local)
{
    $response = "<p>Αυτοματοποιημένο report από amitamotion.gr για τις "
        . date("d-m-Y")
        ."</p><h3>Χρήστες με κωδικούς στη σειρά: "
        . count($suspects_val)
        . "</h3>"
        //. "<h3>Χρήστες με 10 κωδικούς: "
       // . count($limitups_val)
        //. "</h3"
        . "<a href='https://amitamotion.gr/reports/" . $filename . "'>Download report</a>";
    if (! $is_local) {
        $to_email = 'tassos.ioannidis@mrm-mccann.gr, katerina.yiannouli@mrm-mccann.gr, Tiny.Mandouvalou@momentumww.gr, Georgia.Gogoulou@mccann.gr, Sophia.Tsoni@momentumww.gr, Dimitris.Chalatsis@momentumww.gr, george.mitsoulas@mccann.gr';
        $subject = 'amitamotion.gr report ' . date("d-m-Y");
        $message = $response;
        $headers = 'MIME-Version: å1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        $headers .= 'From: report@amitamotion.gr' . "\r\n" .
            'Reply-To: ' . "report@amitamotion.gr" . "\r\n" .
            'X-Mailer: PHP/' . phpversion();


        mail($to_email, $subject, $message, $headers);
    }else{
        echo $response;
    }
}
