<?php return array (
  'facade/ignition' => 
  array (
    'providers' => 
    array (
      0 => 'Facade\\Ignition\\IgnitionServiceProvider',
    ),
    'aliases' => 
    array (
      'Flare' => 'Facade\\Ignition\\Facades\\Flare',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'ktourvas/laravel-admin' => 
  array (
    'providers' => 
    array (
      0 => 'laravel\\admin\\LaravelAdminServiceProvider',
    ),
  ),
  'ktourvas/laravel-auth-journeys' => 
  array (
    'providers' => 
    array (
      0 => 'laravel\\auth\\journeys\\LaravelAuthJourneysServiceProvider',
    ),
  ),
  'ktourvas/laravel-contacts' => 
  array (
    'providers' => 
    array (
      0 => 'laravel\\contacts\\LaravelContactsServiceProvider',
      1 => 'laravel\\contacts\\Providers\\EventServiceProvider',
    ),
  ),
  'ktourvas/laravel-extensions' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\extensions\\LaravelExtensionsServiceProvider',
    ),
  ),
  'ktourvas/laravel-loyalty' => 
  array (
    'providers' => 
    array (
      0 => 'laravel\\loyalty\\LaravelLoyaltyServiceProvider',
    ),
    'aliases' => 
    array (
      'Loyalty' => 'laravel\\loyalty\\Facades\\Loyalty',
    ),
  ),
  'ktourvas/rolesandperms' => 
  array (
    'providers' => 
    array (
      0 => 'ktourvas\\rolesandperms\\RolesAndPermsServiceProvider',
    ),
  ),
  'ktourvas/under-the-cap' => 
  array (
    'providers' => 
    array (
      0 => 'UnderTheCap\\UnderTheCapServiceProvider',
    ),
  ),
  'laravel/passport' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Passport\\PassportServiceProvider',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'laravel/ui' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Ui\\UiServiceProvider',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
  'rap2hpoutre/laravel-log-viewer' => 
  array (
    'providers' => 
    array (
      0 => 'Rap2hpoutre\\LaravelLogViewer\\LaravelLogViewerServiceProvider',
    ),
  ),
);