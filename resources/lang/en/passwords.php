<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

     'reset' => 'To password σου έχει αλλάξει',
    'sent' => 'Μόλις σου στείλαμε email με οδηγίες!',
    'throttled' => 'Παρακαλώ προσπάθησε αργότερα.',
    'token' => 'This password reset token is invalid.',
    'user' => "Δεν υπάρχει χρήστης με αυτό το e-mail.",

];
