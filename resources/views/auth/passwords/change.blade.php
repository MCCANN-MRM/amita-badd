@extends('am::layouts.app')

@section('seo')
    {{-- TODO:  og image and seo attributes --}}
    <title></title>
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" name="description" content="">
    <meta property="og:image" content="{{ url('') }}">
    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:height" content="630"/>
@endsection

@section('bodyClass', 'skin-liftmeup')

@section('content')

    @include('am::app.partials.menu')

    <div class="liftmeup changepassword">
        <div class="brush-top"></div>
        <div class="container changepassword-container paddingforbg paddingForMenu">

            <section class="user-info">
                <div class="d-flex justify-content-center align-items-center">
                    <div class="container">
                        <div class="row">
                            {{--                                 header logo--}}
                            <div class="col-md-12 text-center mt-5 paddingtopforbg">
                                <img src="/assets/cc3e/amita-motion/images/liftmeup/awards/awards_header_logo.png"
                                     class="img-fluid header_logo">
                            </div>

                            {{--                                 avatar and username--}}
                            <div class="col-md-12 text-center mt-5">
                                <div class="avatar-container">
                                    <img src="/assets/cc3e/amita-motion/images/liftmeup/awards/awards_avatar.png"
                                         class="img-fluid avatar-image">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>


            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">{{ __('Change Account Password') }}</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('password.change') }}">
                                @csrf

                                @if( !empty($status) )
                                    <div class="alert alert-warning" role="alert">
                                        {{ $status }}
                                    </div>
                                @endif

                                <div class="form-group row">

                                    <div class="col-md-10 mx-auto">
{{--                                        <label for="password" class="col-md-4 col-form-label">{{ __('Password') }}</label>--}}

                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="password" placeholder="Τρέχων κωδικός*">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-md-10 mx-auto">
{{--                                        <label for="new_password" class="col-md-4 col-form-label">{{ __('New Password') }}</label>--}}

                                        <input id="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" required autocomplete="new-password" placeholder="Νέος κωδικός*">

                                        @error('new_password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">


                                    <div class="col-md-10 mx-auto">
                                        <label for="new_password-confirm" class="col-md-4 col-form-label">{{ __('Confirm New Password') }}</label>

                                        <input id="new_password-confirm" type="password" class="form-control" name="new_password_confirmation" required autocomplete="new-password" placeholder="Επιβεβαίωση νέου κωδικούς*">
                                    </div>

                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary btn-mple">
                                            {{--                                            {{ __('Change') }}--}}
                                            {{ __('Αλλαγή') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="brush-bottom"></div>

    </div>

@endsection
