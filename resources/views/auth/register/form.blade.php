<div class="container register-form">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">

                    <form method="POST" action="{{ route('register') }}" id="registrationForm">
                        @csrf

                        <div class="form-group row">

                            <div class="col-md-10 mx-auto">
                                <label for="name" class="col-form-label text-md-right">
                                    {{ __('ΟΝΟΜΑ*') }}
                                </label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Συμπλήρωσε το όνομά σου">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-10 mx-auto">
                                <label for="email" class="col-form-label text-md-right">{{ __('EMAIL*') }}</label>

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Συμπλήρωσε το email σου">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-10 mx-auto">
                                <label for="password" class="col-form-label text-md-right">{{ __('ΚΩΔΙΚΟΣ*') }}</label>

                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Συμπλήρωσε τον κωδικό σου*">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-5">

                            <div class="col-md-10 mx-auto">
                                <label for="password-confirm" class="col-form-label text-md-right">{{ __('ΕΠΙΒΕΒΑΙΩΣΗ ΚΩΔΙΚΟΥ*') }}</label>

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Επιβεβαίωσε τον κωδικό σου*">
                            </div>
                        </div>
                        <div class="form-check row mb-0">

                            <div class="col-md-10 mx-auto mb-2">
                                <input type="checkbox" class="form-check-input" id="terms" name="optin" required>
                                <label class="form-check-label" for="terms">Συμφωνώ με τους όρους του διαγωνισμού*</label>
                            </div>

                            <div class="col-md-10 mx-auto">
                                <input type="checkbox" class="form-check-input" id="adult" name="optage" required>
                                <label class="form-check-label" for="adult">Είμαι άνω των 18 ετών*</label>
                            </div>

                            <div class="col-md-10 mx-auto mb-2">
                                <input type="checkbox" class="form-check-input" name="optrem" id="remarketing">
                                <label class="form-check-label" for="remarketing">Συμφωνώ να λαμβάνω ενημερώσεις για μελλοντικές ενέργειες της AMITA MOTION και συναινώ στη χρήση των στοιχείων επικοινωνίας μου για τον σκοπό αυτό.</label>
                            </div>

                        </div>

                        <div class="form-group row mb-5">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-pink">
                                    {{ __('ΕΓΓΡΑΦΗ') }}
{{--                                    {{ __('Register') }}--}}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
