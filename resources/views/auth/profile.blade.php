@extends('am::layouts.app')

@section('seo')
    {{-- TODO:  og image and seo attributes --}}
    <title></title>
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" name="description" content="">
    <meta property="og:image" content="{{ url('') }}">
    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:height" content="630"/>
@endsection

@section('bodyClass', 'skin-liftmeup')

@section('content')

    @include('am::app.partials.menu')

    <div class="liftmeup editprofile">

        <div class="brush-top"></div>

        <section class="user-info">
            <div class="d-flex justify-content-center align-items-center">
                <div class="container">
                    <div class="row">
                        {{-- header logo--}}
                        <div class="col-md-12 text-center mt-5">
                            <a href="/playmotion/liftmeup">
                            <img src="/assets/cc3e/amita-motion/images/liftmeup/awards/awards_header_logo.png"
                                 class="img-fluid header_logo">
                            </a>
                        </div>

                        {{-- avatar and username--}}
                        <div class="col-md-12 text-center">
                            <div class="avatar-container">
                                <img src="/assets/cc3e/amita-motion/images/liftmeup/awards/awards_avatar.png"
                                     class="img-fluid avatar-image">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="editprofile-form">
            <div class="d-flex justify-content-center align-items-center ">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        {{-- form--}}
                        <div class="col-md-10 text-center">

                            <form action="/user/profile" method="post">

                                @csrf

                                {{-- //email//--}}
                                <div class="input-group mb-5">
                                    <input type="email" class="form-control" id="editemail" aria-describedby="editemail" placeholder="Email" value="{{ Auth::user()->email }}" name="email">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><img src="/assets/cc3e/amita-motion/images/liftmeup/pencil.png" class="img-fluid pencil"></span>
                                    </div>
                                </div>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                {{-- //name//--}}
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" id="editname" aria-describedby="editname" placeholder="Username" value="{{ Auth::user()->name }}" name="username">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><img src="/assets/cc3e/amita-motion/images/liftmeup/pencil.png" class="img-fluid pencil"></span>
                                    </div>
                                </div>
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                {{-- //name//--}}
{{--                                <div class="input-group mb-5">--}}
{{--                                    <input type="password" class="form-control bluetext" id="newpassword" aria-describedby="newpassword" placeholder="ΝΕΟΣ ΚΩΔΙΚΟΣ" value="" name="newpassword">--}}
{{--                                    <div class="input-group-append">--}}
{{--                                        <span class="input-group-text">--}}
{{--                                            <img src="/assets/cc3e/amita-motion/images/liftmeup/pencil.png" class="img-fluid pencil">--}}
{{--                                        </span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                                {{-- //password verification//--}}
{{--                                <div class="input-group mb-5">--}}
{{--                                    <input type="password" class="form-control bluetext" id="passwordverification" aria-describedby="password verification" placeholder="ΕΠΙΒΕΒΑΙΩΣΗ ΝΕΟΥ ΚΩΔΙΚΟΥ" value="" name="newpassword_confirmation">--}}
{{--                                    <div class="input-group-append">--}}
{{--                                        <span class="input-group-text">--}}
{{--                                            <img src="/assets/cc3e/amita-motion/images/liftmeup/pencil.png" class="img-fluid pencil">--}}
{{--                                        </span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                                <p class="profile-instructions text-left">
                                    Βάλε τον τωρινό κωδικό σου
                                </p>

                                {{-- //existing password//--}}
                                <div class="input-group mb-5 ">
                                    <input type="password" class="form-control" id="existingpassword" aria-describedby="existing password" placeholder="Τρέχων κωδικός" value="" name="password">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <img src="/assets/cc3e/amita-motion/images/liftmeup/pencil.png" class="img-fluid pencil">
                                        </span>
                                    </div>
                                </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <div class="text-center">
                                    <button class="btn btn-yellow btn-big" type="submit">
                                        ΑΠΟΘΗΚΕΥΣΗ
                                    </button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </section>

        <div class="brush-bottom"></div>

    </div>

    @include('am::app.partials.footermusic')

@endsection

@section('js')

@stop
