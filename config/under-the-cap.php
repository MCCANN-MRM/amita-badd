<?php

return [

    'moodmeup' => [

        'id' => 8,

        'name' => 'Mood Me Up',

        'slug' => 'moodmeup',

        /*
        |--------------------------------------------------------------------------
        | Accepting Participation Start Date
        |--------------------------------------------------------------------------
        |
        | This option allows you to specify the date / time after which
        | Participations are being accepted.
        |
        */

        'start_date' => '2020-06-15 00:00:00',

        /*
        |--------------------------------------------------------------------------
        | Accepting Participation End Date
        |--------------------------------------------------------------------------
        |
        | This option allows you to specify the date / time after which
        | Participations are not being accepted.
        |
        */

        'end_date' => '2020-12-20 23:59:00',

        /*
        |--------------------------------------------------------------------------
        | Participation table name
        |--------------------------------------------------------------------------
        |
        | This option allows you to specify the Participation model database
        | $table.
        |
        */

        'participation_table' => 'motion_moodmeup_participations',

        /*
        |--------------------------------------------------------------------------
        | Participation fields
        |--------------------------------------------------------------------------
        |
        | This option allows you to specify the fields that the participation model
        | will use be able to write. The array will be populated at the $fillable
        | protected variable of the Participation model.
        |
        */

        'participation_fields' => [

            'name' => [
                'title' => 'Όνομα',
                'is_searchable' => true,
                'rules' => 'required|max:200',
                'messages' => [
                    'name.required' => 'Το πεδίο Όνομα είναι υποχρεωτικό',
                ],
            ],

            'email' => [
                'title' => 'Email',
                'is_searchable' => true,
                'rules' => 'required|email',
                'messages' => [
                    'email.required' => 'Το πεδίο E-mail είναι υποχρεωτικό',
                    'email.unique' => 'Υπάρχει ήδη συμμετοχή με την συγκεκριμένη e-mail διεύθυνση',
                ],
            ],

            'mood' => [
                'title' => 'Selected Mood',
                'is_searchable' => true,
                'rules' => 'in:1,2,3',
                'messages' => [
                    'mood.digits' => 'Παρακαλούμε συμπλήρωσε ένα σωστό κινητό τηλέφωνο επικοινωνίας',
                    'mood.starts_with' => 'Παρακαλούμε συμπλήρωσε ένα σωστό κινητό τηλέφωνο επικοινωνίας',
                ],
            ],

            'optin' => [
                'title' => 'Αποδοχή Όρων',
                'rules' => 'required|accepted',
                'messages' => [],
            ],

            'optrem' => [
                'title' => 'Αποδοχή Όρων',
                'rules' => '',
                'messages' => [],
            ],

        ],

        /*
        |--------------------------------------------------------------------------
        | RedemptionCode table name
        |--------------------------------------------------------------------------
        |
        | This option allows you to specify the RedemptionCode model database
        | $table.
        |
        */

        'wins_table' => 'motion_moodmeup_wins',

        'redemption_code_table' => 'motion_moodmeup_redemption_codes',

        'participation_presents_table' => 'motion_moodmeup_presents',

        'participation_present_variants_table' => 'motion_moodmeup_present_variants',

        'participation_win_presents_table' => 'motion_moodmeup_win_presents',

        'draws' => [

            1 => [
                'title' => 'MΜU - Κλήρωση 29/06 - Spotify',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 5,
                'runnerups_num' => 5,
            ],

            2 => [
                'title' => 'MΜU - Κλήρωση 29/06 - Προϊόντα',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 4,
                'runnerups_num' => 4,
            ],

            3 => [
                'title' => 'MΜU - Κλήρωση 27/07 - Spotify',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 5,
                'runnerups_num' => 5,
                'restrict' => ['email'],
                'restrict_by_total_participations' => true,
                'filters' => [
                    [
                        'date', 'created_at', '>=', '2020-06-29'
                    ],
                    [
                        'date', 'created_at', '<=', '2020-07-26'
                    ]
                ]
            ],

            4 => [
                'title' => 'MΜU - Κλήρωση 27/07 - Προϊόντα',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 4,
                'runnerups_num' => 4,
                'restrict' => ['email'],
                'restrict_by_total_participations' => true,
                'filters' => [
                    [
                        'date', 'created_at', '>=', '2020-06-29'
                    ],
                    [
                        'date', 'created_at', '<=', '2020-07-26'
                    ]
                ]
            ],

            5 => [
                'title' => 'MΜU - Κλήρωση 31/08 - Spotify',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 5,
                'runnerups_num' => 5,
                'restrict' => ['email'],
                'restrict_by_total_participations' => true,
                'filters' => [
                    [
                        'date', 'created_at', '>=', '2020-07-27'
                    ],
                    [
                        'date', 'created_at', '<=', '2020-08-30'
                    ]
                ]
            ],

            6 => [
                'title' => 'MΜU - Κλήρωση 31/08 - Προϊόντα',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 4,
                'runnerups_num' => 4,
                'restrict' => ['email'],
                'restrict_by_total_participations' => true,
                'filters' => [
                    [
                        'date', 'created_at', '>=', '2020-07-27'
                    ],
                    [
                        'date', 'created_at', '<=', '2020-08-30'
                    ]
                ]
            ],

            7 => [
                'title' => 'MΜU - Κλήρωση 28/09 - Spotify',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 5,
                'runnerups_num' => 5,
                'restrict' => ['email'],
                'restrict_by_total_participations' => true,
                'filters' => [
                    [
                        'date', 'created_at', '>=', '2020-08-31'
                    ],
                    [
                        'date', 'created_at', '<=', '2020-09-27'
                    ]
                ]
            ],

            8 => [
                'title' => 'MΜU - Κλήρωση 28/09 - Προϊόντα',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 4,
                'runnerups_num' => 4,
                'restrict' => ['email'],
                'restrict_by_total_participations' => true,
                'filters' => [
                    [
                        'date', 'created_at', '>=', '2020-08-31'
                    ],
                    [
                        'date', 'created_at', '<=', '2020-09-27'
                    ]
                ]
            ],

            9 => [
                'title' => 'MΜU - Κλήρωση 26/10 - Spotify',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 5,
                'runnerups_num' => 5,
                'restrict' => ['email'],
                'restrict_by_total_participations' => true,
                'filters' => [
                    [
                        'date', 'created_at', '>=', '2020-09-28'
                    ],
                    [
                        'date', 'created_at', '<=', '2020-10-25'
                    ]
                ]
            ],

            10 => [
                'title' => 'MΜU - Κλήρωση 26/10 - Προϊόντα',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 4,
                'runnerups_num' => 4,
                'restrict' => ['email'],
                'restrict_by_total_participations' => true,
                'filters' => [
                    [
                        'date', 'created_at', '>=', '2020-09-28'
                    ],
                    [
                        'date', 'created_at', '<=', '2020-10-25'
                    ]
                ]
            ],

            11 => [
                'title' => 'MΜU - Κλήρωση 23/11 - Spotify',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 5,
                'runnerups_num' => 5,
                'restrict' => ['email'],
                'restrict_by_total_participations' => true,
                'filters' => [
                    [
                        'date', 'created_at', '>=', '2020-10-26'
                    ],
                    [
                        'date', 'created_at', '<=', '2020-11-22'
                    ]
                ]
            ],

            12 => [
                'title' => 'MΜU - Κλήρωση 23/11 - Προϊόντα',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 4,
                'runnerups_num' => 4,
                'restrict' => ['email'],
                'restrict_by_total_participations' => true,
                'filters' => [
                    [
                        'date', 'created_at', '>=', '2020-10-26'
                    ],
                    [
                        'date', 'created_at', '<=', '2020-11-22'
                    ]
                ]
            ],

            13 => [
                'title' => 'MΜU - Κλήρωση 21/12 - Spotify',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 5,
                'runnerups_num' => 5,
                'restrict' => ['email'],
                'restrict_by_total_participations' => true,
                'filters' => [
                    [
                        'date', 'created_at', '>=', '2020-11-23'
                    ],
                    [
                        'date', 'created_at', '<=', '2020-12-20'
                    ]
                ]
            ],

            14 => [
                'title' => 'MΜU - Κλήρωση 21/12 - Προϊόντα',
                'type' => 'adhoc', //adhoc, repeated, instant
                'winners_num' => 4,
                'runnerups_num' => 4,
                'restrict' => ['email'],
                'restrict_by_total_participations' => true,
                'filters' => [
                    [
                        'date', 'created_at', '>=', '2020-11-23'
                    ],
                    [
                        'date', 'created_at', '<=', '2020-12-20'
                    ]
                ]
            ],

        ],

        'participation_stats_table' => 'motion_moodmeup_participation_stats'

    ],

    'liftmeup' => [

        'id' => 9,

        'name' => 'Lift Me Up UTC Promo',

        'slug' => 'liftmeup',

        /*
        |--------------------------------------------------------------------------
        | Accepting Participation Start Date
        |--------------------------------------------------------------------------
        |
        | This option allows you to specify the date / time after which
        | Participations are being accepted.
        |
        */

        'start_date' => '2020-06-15 00:00:00',

        /*
        |--------------------------------------------------------------------------
        | Accepting Participation End Date
        |--------------------------------------------------------------------------
        |
        | This option allows you to specify the date / time after which
        | Participations are not being accepted.
        |
        */

        'end_date' => '2021-12-21 00:00:00',

        /*
        |--------------------------------------------------------------------------
        | Participation table name
        |--------------------------------------------------------------------------
        |
        | This option allows you to specify the Participation model database
        | $table.
        |
        */

        'participation_table' => 'motion_liftmeup_participations',

        /*
        |--------------------------------------------------------------------------
        | Participation fields
        |--------------------------------------------------------------------------
        |
        | This option allows you to specify the fields that the participation model
        | will use be able to write. The array will be populated at the $fillable
        | protected variable of the Participation model.
        |
        */

        'participation_fields' => [

            'user_id' => [
                'title' => 'User',
                'is_searchable' => true,
                'rules' => 'nullable',
                'messages' => ['']
            ],
            
            'code' => [
                'relation' => [
                    'redemptionCode', 'code'
                ],
                'title' => 'Κωδικός',
                'is_searchable' => true,
                'rules' => 'required|max:200',
                'messages' => [
                    'code.required' => 'Το πεδίο είναι υποχρεωτικό',
                ],
            ],
            
           'name' => [
                'title' => 'Όνομα',
                'is_searchable' => true,
                'rules' => 'nullable',
                'messages' => ['']
            ],

            'email' => [
                'title' => 'Email',
                'is_searchable' => true,
                'rules' => 'nullable',
                'messages' => ['']
            ],

        ],

        /*
        |--------------------------------------------------------------------------
        | RedemptionCode table name
        |--------------------------------------------------------------------------
        |
        | This option allows you to specify the RedemptionCode model database
        | $table.
        |
        */

        'wins_table' => 'motion_liftmeup_wins',

        'redemption_code_table' => 'motion_liftmeup_redemption_codes',

        'participation_presents_table' => 'motion_liftmeup_presents',

        'participation_present_variants_table' => 'motion_liftmeup_present_variants',

        'participation_win_presents_table' => 'motion_liftmeup_win_presents',

        'draws' => [],

        'participation_stats_table' => 'motion_liftmeup_participation_stats'

    ],

];
