<?php

return [

    'root_url' => 'admin',

    'iprestrict' => false,

    /*
    |--------------------------------------------------------------------------
    | Use default Contacts Screens
    |--------------------------------------------------------------------------
    |
    | This option allows you to specify if the installation will be using the
    | default Contacts Management Screens. This option requires the
    | ktourvas/laravel-contacts package to be installed.
    |
    */

    'sidebar_includes' => [],

    'dashboard' => [
        'blocks' => [

        ],
    ],

    'front' => [

        'meta' => [
            'title' => 'MRM//MCCANN Administrative Utilities'
        ],

        'logo' => [

            'image' => '/assets/vendor/ladmin/images/MRM_McCann_2Col_Logo_2014-RGB.svg',

            'alt' => 'MRM//MCCANN Logo'

        ],

        'footer' => [

            'right' => 'MRM//MCCANN Administrative Utilities'

        ]

    ]

];
