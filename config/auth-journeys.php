<?php

return [

    'ux' => [

        'login' => [

            'redirectTo' => '/',

            'view' => 'am::app.sections.auth.login',

            'maxAttempts' => 50,

            'decayMinutes' => 10,
        ],

        'register' => [

            'allowset' => false, // allow only previously set users to register

            'view' => 'am::app.sections.auth.register'

        ],

        'password' => [

            'change' => 'am::app.sections.auth.passwords.change',

            'email' => 'am::app.sections.auth.passwords.email',

            'reset' => 'am::app.sections.auth.passwords.reset',

        ],

    ],

    /**
     *
     * role rules. array keys are checked against user roles for matching in order for rules
     * to be applied. Currently change policy only implemented.
     *
     */
    'roles' => [

        'default' => [

            'complexity' => 0, // 0. min 8, 1. min 8, nums, letters, special, 2. min 8, nums, letters, capital letters, special,

            'changepolicy' => 'none', // none, days

            'inactivitylogout' => false,

            'redirectTo' => '/playmotion/liftmeup',

        ],

        'admin' => [

            'redirectTo' => '/admin',

            'complexity' => 2, // 1. min 8, 2. min 8, nums, letters, special, 3. min 8, nums, letters, capital letters, special,

            'changepolicy' => 'days', // none, days

            'days' => 90,

            'inactivitylogout' => true,

            'logoutafter' => 1800 // seconds

        ]

    ],

    'user' => [
        'fields' => [
            'optin' => [
                'rules' => [ 'required', 'accepted' ],
                'messages' => [
                    'optin.required' => '',
                ],
            ],
            'optrem' => [
                'rules' => [],
                'messages' => [
                    'optrem.required' => '',
                ],
            ],
            'optage' => [
                'rules' => [ 'required', 'accepted' ],
                'messages' => [
                    'optage.required' => '',
                ],
            ],
        ]
    ]

];
