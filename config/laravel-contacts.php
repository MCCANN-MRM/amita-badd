<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contact table name
    |--------------------------------------------------------------------------
    |
    | This option allows you to specify the Contact model database
    | $table.
    |
    */

    'contacts_table' => 'lc_contacts',

    'submit' => [

        'rules' => [
            'subject' => 'required',
            'name' => 'required|max:200',
//            'surname' => 'required|max:200',
            'email' => 'required|email',
            'msg' => 'required|max:1000',
        ],
        'messages' => [
            'subject.required' => 'Το πεδίο Θέμα είναι υποχρεωτικό',
            'name.required' => 'Το πεδίο Όνομα είναι υποχρεωτικό',
//            'surname.required' => 'Το πεδίο Επώνυμο είναι υποχρεωτικό',
            'email.required' => 'Το πεδίο E-mail είναι υποχρεωτικό',
            'msg.required' => 'Παρακαλούμε συμπλήρωσε το μήνυμά σας',
        ]

    ],

    // mailing has been moved to the new submission listener on amita-motion package
    //    'mail_contacts_to' => 'info@amitamotion.gr'

];
