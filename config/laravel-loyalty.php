<?php

return [

    'auto_register' => true, // registers automatically user to program when one program exists

    'ux' => [
        'transactions' => [
            'descriptions' => [
                1 => 'Present purchase',
                2 => 'Draw participation purchase'
            ]
        ]
    ],

];