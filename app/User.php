<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use ktourvas\rolesandperms\Entities\HasPersmissions;
use ktourvas\rolesandperms\Entities\HasRoles;
use laravel\auth\journeys\Entities\HasAuthJourneys;
use laravel\auth\journeys\Entities\HasCustomPasswordResetNotification;
use laravel\auth\journeys\Entities\hasPasswordsHistory;
use laravel\loyalty\Entities\HasLoyalty;
use Laravel\Passport\HasApiTokens;
use laravel\admin\Entities\Adminable;
use UnderTheCap\Entities\Participant;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, HasPersmissions, Adminable,
        hasPasswordsHistory, HasCustomPasswordResetNotification,
        HasAuthJourneys, HasLoyalty, Participant;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'optin', 'optrem', 'optage'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
