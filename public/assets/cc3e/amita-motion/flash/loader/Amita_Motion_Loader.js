(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.Tween21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F6A500").s().p("AzRbgMAAAguRQFKtfG8HWQEPEfEwALQDBAGDOhoQIVkMC6HNMAAAAuRg");
	this.shape.setTransform(0,0.0037);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-123.4,-176,246.8,352.1);


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EE7D00").s().p("AqqLPQgSgPAAgeIACgUIAcibQFsloE8noQjBAYhAhqQgUghgFgqIgBgjIOHi6QAjgHASAQQASAOAAAfIgCAUIhYHtQgKA0gqAtQgsAtgyAKIlqBFQgRAEgLALQgNAMgDARIhCF6QgKA0gqAtQgrAtgzAKIndBdQgKACgJAAQgUAAgNgLg");
	this.shape.setTransform(0.0312,11.3247,0.8227,0.8227);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EE7D00").s().p("AhRBGQAMhEAig1QAhg1AhgHQAjgGAPArQAQAsgMBEIgIAqIimAhg");
	this.shape_1.setTransform(6.8177,-62.1268,0.8227,0.8227);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-57.6,-71.4,115.30000000000001,142.8);


(lib.Tween19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D22E7D").s().p("AhZh+IBDgOIAEDMIBrgVIABA/IiuAjg");
	this.shape.setTransform(0,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9,-14,18,28.1);


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D22E7D").s().p("AgPiRIAVgEIB7D5IhHANIgKgUIhaASIgKAYIhMAPgAgTAyIAqgIIgXg3g");
	this.shape.setTransform(0,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.8,-15,25.700000000000003,30.1);


(lib.Tween17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D22E7D").s().p("AgdAuIhdiSIBDgNIA3BTIAthnIBOgQIhWC4IACBlIhCANg");
	this.shape.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.2,-14.9,24.5,29.9);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#63C8D2").s().p("AiGhtIAWgFIBwB1IBsigIAVgEIAGEQIhEANIgChqIgvBEIgVAEIg8g7IACB2IhDANg");
	this.shape.setTransform(0,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.4,-16.2,26.9,32.5);


(lib.Tween15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#63C8D2").s().p("AhSB3QgrgcgLg3QgMg7AkgyQAjgyA+gMQA3gLArAcQAsAdAKA2QAMA7gjAyQgkAyg9AMQgQADgOAAQglAAgggUgAgIhIQggAGgRAaQgRAbAGAgQAGAfAWAOQAWAPAbgGQAggGARgaQARgbgGggQgGgfgWgOQgPgLgUAAIgOACg");
	this.shape.setTransform(0.0359,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14,-13.9,28.1,27.9);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#63C8D2").s().p("Aggg4IhOAPIgBg/IDdgrIACA+IhNAQIADDMIhCANg");
	this.shape.setTransform(0,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.2,-14.8,22.4,29.700000000000003);


(lib.Tween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#63C8D2").s().p("Agkh+IBDgOIAFELIhCAOg");
	this.shape.setTransform(0,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.6,-14,7.300000000000001,28.1);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#63C8D2").s().p("AhSB3QgsgdgKg2QgMg7AjgyQAkgyA9gMQA3gLAsAcQArAcALA3QAMA7gkAyQgjAyg+AMQgPADgOAAQgmAAgfgUgAgIhIQggAGgRAaQgRAbAGAgQAGAfAWAOQAVAPAcgGQAggGARgaQARgbgGggQgGgfgWgOQgQgLgTAAIgOACg");
	this.shape.setTransform(0.0141,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14,-13.9,28.1,27.9);


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#63C8D2").s().p("Ah1hzIAWgFICOB1IgCiLIBDgOIAGEQIgWAFIiOh3IADCNIhEAOg");
	this.shape.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.7,-15.6,23.5,31.299999999999997);


(lib.Tween10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F1A73B").s().p("AhZh+IBDgOIAEDMIBrgVIABA/IiuAjg");
	this.shape.setTransform(0.025,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9,-14,18.1,28.1);


(lib.Tween9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F1A73B").s().p("AhSB3QgsgcgKg3QgMg7AjgyQAkgyA9gMQA3gLAsAcQArAdALA2QAMA7gkAyQgjAyg+AMQgPADgOAAQgmAAgfgUgAgIhIQggAGgRAaQgRAbAGAgQAGAfAWAOQAVAPAcgGQAggGARgaQARgbgGggQgGgegWgPQgQgLgTAAIgOACg");
	this.shape.setTransform(0.0141,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14,-13.9,28.1,27.9);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F1A73B").s().p("AgPiRIAVgEIB7D5IhHAOIgKgVIhaASIgJAYIhNAPgAgTAyIAqgIIgXg3g");
	this.shape.setTransform(0,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.8,-15,25.700000000000003,30.1);


(lib.Tween7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F1A73B").s().p("Ah1h8IBOgPQA5gLArAaQAsAbAKA0QAMA8gjAvQgkAwhCANIhmAUgAgghNIgQADIADCNIAcgGQAigHASgZQASgZgHggQgFgdgVgNQgOgJgTAAQgJAAgKACg");
	this.shape.setTransform(0.0032,0.0137);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.8,-14.3,23.6,28.700000000000003);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F1A73B").s().p("Agkh+IBDgOIAFELIhCAOg");
	this.shape.setTransform(0,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.6,-14,7.300000000000001,28.1);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F1A73B").s().p("Ah1hzIAWgFICOB0IgCiLIBDgNIAGEQIgWAFIiOh3IADCNIhEAOg");
	this.shape.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.7,-15.6,23.5,31.299999999999997);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F1A73B").s().p("AhNB3QgrgcgLg3QgMg7AjgyQAjgyA/gMQBMgPAvA4IgsAwQgfgignAHQgiAHgRAaQgRAaAGAiQAGAfAWAPQAWAPAegGQAhgGASgaIAAgPIg9AMIgBg7ICAgZIACBbQgOAigeAXQgeAYgnAIQgRADgOAAQgmAAgfgUg");
	this.shape.setTransform(-0.0016,0.0183);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.5,-13.9,27.1,27.9);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D22E7D").s().p("Ahmh7IBagRQAsgJAfARQAeASAIAmQAJArgbAgQgbAggzAKIgkAHIACBTIhEANgAgBhRIggAGIABA/IAlgIQAQgDAHgKQAIgKgDgPQgDgOgJgGQgHgEgIAAIgHABg");
	this.shape.setTransform(0.0268,0.0222);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.3,-14.4,20.700000000000003,28.9);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EAA83A").s().p("AoHPCQgdgnALg8IBDl3IlrBHQgPADgOAAQg1gBgdglQgdgnALg8IBYnsQAMhEA3g6QA3g4BCgOIFrhGIBDl3QAMhEA3g5QA3g6BCgNIHchcQAMgDARAAQA1AAAdAmQAdAngLA7IhCF1IFrhHQAPgCAOAAQA2gBAcAnQAdAmgLA8IhYHsQgMBEg3A5Qg3A5hCANIlrBHIhEF6QgMBEg3A5Qg3A5hCANInbBdQgQADgNAAQg2AAgdgmgAnyNmQgHAtAUAZQARAUAgAAQAIAAANgCIHbhdQA2gLAugwQAvgwAKg4IBJmVIGFhMQA2gKAugxQAvgwAKg4IBYnsQAIgtgVgZQgQgTgggBIgWACImkBTIBNmvQAIgtgVgZQgQgTggAAQgMAAgKACInbBcQg2ALguAxQgvAvgKA4IhIGTImFBMQg2AKguAxQgvAwgKA4IhYHsQgIAsAVAZQAQAVAggBQAJAAANgCIGkhSg");
	this.shape.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-96.6,-100,193.3,200);


// stage content:
(lib.Amita_Motion_Loader = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// G
	this.instance = new lib.Tween4("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(199.95,246.3,0.005,0.005,0,0,0,10,10);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({regX:0,regY:0,scaleX:0.8227,scaleY:0.8227,x:253.15,y:245.65},9).to({y:245.6},35).to({y:245.65,alpha:0},5).wait(8));

	// N
	this.instance_1 = new lib.Tween5("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(199.85,246.35,0.005,0.005,0,0,0,10,20.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(59).to({_off:false},0).to({regX:0,regY:0,scaleX:0.8227,scaleY:0.8227,x:231.3,y:250.35},9).to({x:231.25},38).to({x:231.3,alpha:0},5).wait(8));

	// I
	this.instance_2 = new lib.Tween6("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(199.7,246.35,0.005,0.005,0,0,0,0,10);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(54).to({_off:false},0).to({regY:0,scaleX:0.8227,scaleY:0.8227,x:216.7,y:252.85},10).to({regX:0.1,regY:0.1,x:216.8,y:252.9},42).to({regX:0,regY:0,x:216.7,y:252.85,alpha:0},5).wait(8));

	// D
	this.instance_3 = new lib.Tween7("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(199.65,246.35,0.005,0.005,0,0,0,10,10);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(51).to({_off:false},0).to({regX:0,regY:0,scaleX:0.8227,scaleY:0.8227,x:202.55,y:256.8},9).to({y:256.75},46).to({y:256.8,alpha:0},5).wait(8));

	// A
	this.instance_4 = new lib.Tween8("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(199.5,246.35,0.005,0.005);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(47).to({_off:false},0).to({scaleX:0.8227,scaleY:0.8227,x:182.25,y:260.65},9).to({regX:0.1,regY:0.1,x:182.3,y:260.7},50).to({regX:0,regY:0,x:182.25,y:260.65,alpha:0},5).wait(8));

	// O
	this.instance_5 = new lib.Tween9("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(199.4,246.4,0.005,0.005,0,0,0,10,10);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(43).to({_off:false},0).to({regX:0,regY:0,scaleX:0.8227,scaleY:0.8227,x:160.55,y:264.3},9).to({regX:0.1,regY:0.1,x:160.65,y:264.4},54).to({regX:0,regY:0,x:160.55,y:264.3,alpha:0},5).wait(8));

	// L
	this.instance_6 = new lib.Tween10("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(199.25,246.4,0.005,0.005);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(40).to({_off:false},0).to({scaleX:0.8227,scaleY:0.8227,x:142.35,y:268.55},9).to({regX:0.1,regY:0.1,x:142.4,y:268.6},57).to({regX:0,regY:0,x:142.35,y:268.55,alpha:0},5).wait(8));

	// N
	this.instance_7 = new lib.Tween11("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(199.7,246.15,0.005,0.005,0,0,0,10,0);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(60).to({_off:false},0).to({regX:0,scaleX:0.8227,scaleY:0.8227,x:211.05,y:225.25},10).to({x:211,y:225.2},36).to({x:211.05,y:225.25,alpha:0},5).wait(8));

	// O
	this.instance_8 = new lib.Tween15("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(188.85,229.7,0.0146,0.0148);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(57).to({_off:false},0).to({scaleX:0.8227,scaleY:0.8227},9).to({startPosition:0},40).to({alpha:0},5).wait(8));

	// I
	this.instance_9 = new lib.Tween13("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(163.7,234.1,0.0135,0.0131,0,0,0,0,3.8);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(53).to({_off:false},0).to({regY:0,scaleX:0.8227,scaleY:0.8227,x:173.1,y:232.75},10).to({startPosition:0},43).to({alpha:0},5).wait(8));

	// T
	this.instance_10 = new lib.Tween14("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(163.5,234.15,0.0135,0.0131,0,0,0,0,3.8);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(50).to({_off:false},0).to({regY:0,scaleX:0.8227,scaleY:0.8227,x:160.25,y:234.8},9).to({x:160.2},47).to({x:160.25,alpha:0},5).wait(8));

	// O
	this.instance_11 = new lib.Tween12("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(140.5,239.35,0.0146,0.0148,0,0,0,0,3.4);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(47).to({_off:false},0).to({regY:0,scaleX:0.8227,scaleY:0.8227,y:239.3},9).to({regX:0.1,regY:0.1,x:140.55,y:239.35},50).to({regX:0,regY:0,x:140.5,y:239.3,alpha:0},5).wait(8));

	// M
	this.instance_12 = new lib.Tween16("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(116.6,246.55,0.0153,0.0127,0,0,0,3.2,7.9);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(43).to({_off:false},0).to({regX:0.1,regY:3.4,scaleX:0.8227,scaleY:0.8227,y:246.6},9).to({regX:0.2,y:246.55},54).to({regX:0.1,y:246.6,alpha:0},5).wait(8));

	// Y
	this.instance_13 = new lib.Tween17("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(95.2,247.35,0.0168,0.0137,0,0,0,3,3.6);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(40).to({_off:false},0).to({regX:0,regY:0,scaleX:0.8227,scaleY:0.8227,x:95.15},9).to({regX:0.1,regY:0.1,x:95.25,y:247.45},57).to({regX:0,regY:0,x:95.15,y:247.35,alpha:0},5).wait(8));

	// A
	this.instance_14 = new lib.Tween18("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(79.5,252,0.016,0.0137,0,0,0,6.2,3.6);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(36).to({_off:false},0).to({regX:0,regY:0,scaleX:0.8227,scaleY:0.8227,x:79.45},10).to({regX:0.1,regY:0.1,x:79.5,y:252.1},60).to({regX:0,regY:0,x:79.45,y:252,alpha:0},5).wait(8));

	// L
	this.instance_15 = new lib.Tween19("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(62.2,255.5,0.0228,0.0146);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(34).to({_off:false},0).to({scaleX:0.8227,scaleY:0.8227},9).to({startPosition:0},63).to({alpha:0},5).wait(8));

	// P
	this.instance_16 = new lib.Tween3("synched",0);
	this.instance_16.parent = this;
	this.instance_16.setTransform(45.05,259.05,0.0199,0.0143,0,0,0,2.5,3.5);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(31).to({_off:false},0).to({regX:0,regY:0,scaleX:0.8227,scaleY:0.8227,x:45,y:259},9).to({x:45.05},66).to({x:45,alpha:0},5).wait(8));

	// Paint (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AzRCgIAAijQCvgWIFhRQFMg0FFgBQCzAACyAPQHzAqEGBiIAACkg");
	var mask_graphics_1 = new cjs.Graphics().p("AzRHMIAAnGQDDlpHABRQGEBGE+iHQAwgVAugZQFji/KdEwIAALcg");
	var mask_graphics_2 = new cjs.Graphics().p("AzRHcIAAoAQA9iPBmgiQBNgaBLggQAUgHATgKQCbhDDWAlQFxBAEyh2QAvgTAtgWQFViqJ8EkIAAL/g");
	var mask_graphics_3 = new cjs.Graphics().p("AzRHtIAAo7QBHjACJgFQBngEBagfQAagIAYgMQCThCDMAiQFeA8EphnQAsgQArgUQFJiUJaEZIAAMhg");
	var mask_graphics_4 = new cjs.Graphics().p("AzRH+IAAp3QBQjwCtAZQCCASBogfQAggIAcgPQCNhADCAfQFKA2EfhXQAqgNApgRQE7iAI6EOIAANEg");
	var mask_graphics_5 = new cjs.Graphics().p("AzRIQIAAqzQBakhDRA2QCbAoB4geQAkgIAhgSQCGg+C5AcQE2AwEVhGQApgLAngOQEuhrIYEDIAANng");
	var mask_graphics_6 = new cjs.Graphics().p("AzRIhIAAruQBllSDzBUQC1A+CGgeQArgIAlgVQB/g7CwAYQEiArEMg2QAmgJAkgMQEihVH3D3IAAOKg");
	var mask_graphics_7 = new cjs.Graphics().p("AzRI0IAAsqQBvmDEWByQDPBUCWgeQAxgIApgYQB4g5CmAVQEOAmEBgmQAkgHAkgJQEVhAHVDsIAAOtg");
	var mask_graphics_8 = new cjs.Graphics().p("AzRJXIAAtlQB4m0E6CQQDqBqCkgeQA3gJAtgZQBxg4CcATQD8AfD3gVQAhgFAigGQEIgrG0DhIAAPPg");
	var mask_graphics_9 = new cjs.Graphics().p("AzRKDIAAugQCCnlFdCtQEECBCzgeQA8gJAygdQBqg1CSAPQDqAaDrgFIBBgFQD6gXGTDWIAAPyg");
	var mask_graphics_10 = new cjs.Graphics().p("AzRKwIAAvcQCMoWGADLQEeCXDDgeQBBgJA3ggQBjgyCIALQDWAVDiALIA8gBQDugBFxDKIAAQWg");
	var mask_graphics_11 = new cjs.Graphics().p("AzRLdIAAwYQCWpHGkDpQE4CtDRgdQBHgKA7giQBbgxCAAJQDCAPDYAcIA4AEQDiATFPDAIAAQ4g");
	var mask_graphics_12 = new cjs.Graphics().p("AzRMJIAAxTQCgp3HHEGQFTDDDfgdQBMgKA/glQBWguB2AFQCuAKDOArIA0AKQDUAoEvC0IAARbg");
	var mask_graphics_13 = new cjs.Graphics().p("AzRM2IAAyPQCqqoHqEkQFtDZDtgdQBTgKBDgnQBPgtBtACQCaAFDFA7IAvAOQDIA+ENCpIAAR+g");
	var mask_graphics_14 = new cjs.Graphics().p("AzRNjIAAzKQC0raINFCQGHDvD9gcQBYgKBIgrQEVijKpHGIAAShg");
	var mask_graphics_15 = new cjs.Graphics().p("AzROXIAA1GQCtqzHtEiQFwDVDxgfQBTgKBFgpQBWgyB4AUQCDAYCoA6ICwA8QCzA0C0CQIAAUgg");
	var mask_graphics_16 = new cjs.Graphics().p("AzRPLIAA3CQClqMHOECQFZC7DlghQBPgLBAgmQBgg2B+AkIFBBjQBcAaBlAWQDOAbC1CpIAAWeg");
	var mask_graphics_17 = new cjs.Graphics().p("AzRQAIAA4+QCdpmGuDiQFEChDYgjQBKgMA9gkQBng5CIA1QCYBAC+AzQBjAYBvALQDpADC1DCIAAYdg");
	var mask_graphics_18 = new cjs.Graphics().p("AzRQ1IAA66QCWpAGODCQEsCIDNgmQBGgMA5giQBvg9CPBGQCkBUDIAvQBsAXB3ABQEEgWC2DaIAAacg");
	var mask_graphics_19 = new cjs.Graphics().p("AzRRqIAA82QCOoZFuCiQEXBtDAgnQBBgNA2ggQB4hBCWBXQCuBoDTAsQB1AVB/gKQEgguC2DyIAAcbg");
	var mask_graphics_20 = new cjs.Graphics().p("AzRSgIAA+yQCGnzFOCCQEBBUCzgqQA9gNAzgeQCAhECeBmQC5B9DdAoQB9AUCJgUQE7hHC2EKIAAeag");
	var mask_graphics_21 = new cjs.Graphics().p("AzRToMAAAgguQB/nMEuBiQDqA6CngtQA5gNAvgbQCIhJCmB3QDDCSDoAkQCGASCSgfQFVhgC3EjMAAAAgZg");
	var mask_graphics_22 = new cjs.Graphics().p("AzRUwMAAAgiqQB3mmEOBCQDUAgCbguQAzgOAtgZQCQhNCuCIQDOCmDyAgQCOARCbgqQFwh4C4E7MAAAAiYg");
	var mask_graphics_23 = new cjs.Graphics().p("AzRV4MAAAgkmQBwmADuAiQC9AHCPgxQAvgPAogWQCZhQC3CYQDYC6D8AdQCXAOCjgzQGMiRC4FTMAAAAkXg");
	var mask_graphics_24 = new cjs.Graphics().p("AzRXAMAAAgmiQBolZDOACQCngUCDgzQAqgPAlgUQChhUC+CpQDkDOEGAZQCfANCsg+QGniqC5FsMAAAAmWg");
	var mask_graphics_25 = new cjs.Graphics().p("AzRYIMAAAgodQBhkzCugeQCQguB3g1QAlgPAhgTQCqhXDGC5QDvDiEQAWQCoAMC1hJQHCjCC5GEMAAAAoUg");
	var mask_graphics_26 = new cjs.Graphics().p("AzRZQMAAAgqZQBYkNCQg+QB6hHBqg3IA+ghQCyhbDODKQD6D3EbASQCwAKC9hUQHdjbC6GdMAAAAqTg");
	var mask_graphics_27 = new cjs.Graphics().p("AzRaYMAAAgsVQBRjmBvheQBjhhBfg6IA2gfQC5hfDWDbQEGELElAOQC5AIDFhdQH5j0C6G1MAAAAsSg");
	var mask_graphics_28 = new cjs.Graphics().p("AzRbgMAAAguRQFKtfG8HWQEPEfEwALQDBAGDOhoQIVkMC6HNMAAAAuRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:142.25,y:262.1242}).wait(1).to({graphics:mask_graphics_1,x:142.25,y:232.1363}).wait(1).to({graphics:mask_graphics_2,x:142.25,y:230.4723}).wait(1).to({graphics:mask_graphics_3,x:142.25,y:228.7828}).wait(1).to({graphics:mask_graphics_4,x:142.25,y:227.0776}).wait(1).to({graphics:mask_graphics_5,x:142.25,y:225.3359}).wait(1).to({graphics:mask_graphics_6,x:142.25,y:223.5524}).wait(1).to({graphics:mask_graphics_7,x:142.25,y:221.7328}).wait(1).to({graphics:mask_graphics_8,x:142.25,y:218.1983}).wait(1).to({graphics:mask_graphics_9,x:142.25,y:213.7643}).wait(1).to({graphics:mask_graphics_10,x:142.25,y:209.3174}).wait(1).to({graphics:mask_graphics_11,x:142.25,y:204.8485}).wait(1).to({graphics:mask_graphics_12,x:142.25,y:200.3962}).wait(1).to({graphics:mask_graphics_13,x:142.25,y:195.9162}).wait(1).to({graphics:mask_graphics_14,x:142.25,y:191.4447}).wait(1).to({graphics:mask_graphics_15,x:142.25,y:186.2136}).wait(1).to({graphics:mask_graphics_16,x:142.25,y:180.9696}).wait(1).to({graphics:mask_graphics_17,x:142.25,y:175.7098}).wait(1).to({graphics:mask_graphics_18,x:142.25,y:170.4311}).wait(1).to({graphics:mask_graphics_19,x:142.25,y:165.1241}).wait(1).to({graphics:mask_graphics_20,x:142.25,y:159.703}).wait(1).to({graphics:mask_graphics_21,x:142.25,y:152.5276}).wait(1).to({graphics:mask_graphics_22,x:142.25,y:145.323}).wait(1).to({graphics:mask_graphics_23,x:142.25,y:138.1207}).wait(1).to({graphics:mask_graphics_24,x:142.25,y:130.9055}).wait(1).to({graphics:mask_graphics_25,x:142.25,y:123.6978}).wait(1).to({graphics:mask_graphics_26,x:142.25,y:116.4869}).wait(1).to({graphics:mask_graphics_27,x:142.25,y:109.2606}).wait(1).to({graphics:mask_graphics_28,x:142.25,y:102.0537}).wait(91));

	// Layer_1
	this.instance_17 = new lib.Tween20("synched",0);
	this.instance_17.parent = this;
	this.instance_17.setTransform(166.35,133);

	var maskedShapeInstanceList = [this.instance_17];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_17).to({startPosition:0},109).to({alpha:0},7).wait(3));

	// Mask (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AhIP7QgfggAKg4IA3k1IkqA6Qg2ALggggQgfghAKg4IBJmVQAKg4AtgwQAtguA2gLIEqg5IA3k1QAKg4AugvQAtgvA2gKIGIhNQA2gKAfAgQAfAggJA4Ig3EzIErg6QA2gLAfAhQAfAggKA4IhIGUQgKA4gtAvQguAvg2ALIkrA6Ig4E3QgJA4guAvQgtAvg2AKImIBNQgNACgMAAQgkAAgYgYg");
	mask_1.setTransform(114.3598,104.3414);

	// Paint
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F6A500").s().p("AzRCgIAAijQCvgWIFhRQFMg0FFgBQCzAACyAPQHzAqEGBiIAACkg");
	this.shape.setTransform(142.25,262.1242);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F6A500").s().p("AzRHMIAAnGQDDlpHABRQGEBGE+iHQAwgVAugZQFji/KdEwIAALcg");
	this.shape_1.setTransform(142.25,232.1363);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F6A500").s().p("AzRHcIAAoAQA8iPBngiQBNgaBLggQAUgHATgJQCahEDXAlQFxBAEzh2QAugTAtgWQFViqJ8ElIAAL+g");
	this.shape_2.setTransform(142.25,230.4687);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F6A500").s().p("AzRHtIAAo7QBGjACKgFQBngEBagfQAagIAYgMQCThCDNAiQFdA7EphmQAsgQArgUQFIiUJbEZIAAMhg");
	this.shape_3.setTransform(142.25,228.7827);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F6A500").s().p("AzRH+IAAp3QBQjwCuAZQCBASBogfQAggIAcgPQCMhADDAfQFLA2EehXQAqgNApgRQE8iAI5EOIAANEg");
	this.shape_4.setTransform(142.25,227.0739);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F6A500").s().p("AzRIQIAAqzQBakhDRA2QCbAoB3geQAmgJAggRQCGg+C5AcQE2AwEVhGQAogLAngOQEvhrIYECIAANog");
	this.shape_5.setTransform(142.25,225.336);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F6A500").s().p("AzRIhIAAruQBklSD0BUQC1A/CHgfQArgIAkgVQB/g7CwAYQEiArELg2QAmgJAmgLQEhhWH3D3IAAOKg");
	this.shape_6.setTransform(142.25,223.5608);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F6A500").s().p("AzRI0IAAsqQBumDEXByQDQBUCVgeQAxgJApgXQB4g5CmAVQEPAmEAgmQAkgHAkgJQEVhAHVDsIAAOtg");
	this.shape_7.setTransform(142.25,221.7362);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F6A500").s().p("AzRJXIAAtlQB4m0E6CQQDqBqCkgeQA2gJAugaQBxg3CdASQD7AgD3gVQAigFAhgGQEIgsG0DhIAAPQg");
	this.shape_8.setTransform(142.25,218.199);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F6A500").s().p("AzRKDIAAugQCCnlFeCtQEDCBCzgeQA8gJAygdQBqg1CSAPQDpAbDtgGIA/gFQD8gXGSDWIAAPyg");
	this.shape_9.setTransform(142.25,213.7618);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F6A500").s().p("AzRKwIAAvcQCMoWGBDLQEeCXDCgeQBBgJA2gfQBkgzCIALQDWAVDiALIA8gBQDugBFxDLIAAQVg");
	this.shape_10.setTransform(142.25,209.3121);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F6A500").s().p("AzRLcIAAwXQCWpHGkDpQE4CtDRgdQBHgKA7giQBbgxCAAJQDCAPDYAbIA4AEQDhAUFQC/IAAQ4g");
	this.shape_11.setTransform(142.25,204.8536);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#F6A500").s().p("AzRMJIAAxTQCgp4HHEHQFSDDDggdQBMgKA/glQBWgvB2AGQCuAKDOArIA0AJQDVApEuC0IAARbg");
	this.shape_12.setTransform(142.25,200.3885);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#F6A500").s().p("AzRM2IAAyPQCqqoHqEkQFtDZDtgdQBTgKBDgnQBPgtBtACQCbAFDDA7IAwAOQDIA+ENCpIAAR+g");
	this.shape_13.setTransform(142.25,195.9185);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F6A500").s().p("AzRNjIAAzKQC0raINFCQGHDvD9gcQBYgKBIgrQEVijKpHGIAAShg");
	this.shape_14.setTransform(142.25,191.4447);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F6A500").s().p("AzROXIAA1GQCsqzHuEiQFwDVDwgfQBUgKBEgpQBXgyB4AUQCDAYCoA6ICwA8QCzA0C0CQIAAUgg");
	this.shape_15.setTransform(142.25,186.2122);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#F6A500").s().p("AzRPLIAA3CQClqMHOECQFZC7DlghQBOgLBBgmQBfg2CAAlIFABiQBcAaBmAWQDNAbC1CpIAAWeg");
	this.shape_16.setTransform(142.25,180.9669);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#F6A500").s().p("AzRQAIAA4+QCdpmGuDiQFDChDZgjQBLgLA8gkQBng6CHA1QCZBAC9AzQBlAZBuALQDpADC1DBIAAYdg");
	this.shape_17.setTransform(142.25,175.7056);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#F6A500").s().p("AzRQ1IAA66QCWpAGODCQEsCIDNgmQBGgMA5giQBvg9CPBGQCkBUDHAvQBtAXB3ABQEEgWC2DaIAAacg");
	this.shape_18.setTransform(142.25,170.4237);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#F6A500").s().p("AzRRqIAA82QCOoZFuCiQEXBtDAgnQBBgNA2gfQB4hCCWBXQCuBoDTAsQB1AVCAgKQEfguC2DyIAAcbg");
	this.shape_19.setTransform(142.25,165.1153);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#F6A500").s().p("AzRSgIAA+yQCGnzFOCCQEBBUC0gqQA8gNAzgdQCAhFCeBnQC5B9DdAnQB9AUCJgUQE6hHC3EKIAAeag");
	this.shape_20.setTransform(142.25,159.7063);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#F6A500").s().p("AzRToMAAAgguQB/nMEuBiQDqA6CngtQA4gNAwgbQCIhJCmB3QDDCSDoAkQCGASCRgfQFWhgC3EjMAAAAgZg");
	this.shape_21.setTransform(142.25,152.5169);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#F6A500").s().p("AzRUwMAAAgiqQB3mmEOBCQDUAgCbguQAzgOAtgZQCQhNCvCIQDNCmDyAgQCPARCagqQFwh4C4E7MAAAAiYg");
	this.shape_22.setTransform(142.25,145.3193);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#F6A500").s().p("AzRV4MAAAgkmQBwl/DuAiQC9AGCPgxQAvgOAogXQCZhQC2CYQDZC6D8AdQCXAPCjg0QGMiRC4FTMAAAAkXg");
	this.shape_23.setTransform(142.25,138.1157);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#F6A500").s().p("AzRXAMAAAgmhQBolaDOACQCngUCDgzQAqgPAlgUQChhUC+CpQDjDOEHAZQCgANCrg+QGnipC5FrMAAAAmWg");
	this.shape_24.setTransform(142.25,130.9075);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#F6A500").s().p("AzRYIMAAAgodQBgkzCvgeQCQguB3g1QAlgQAigSQCphXDGC5QDvDiEQAWQCoALC1hIQHCjDC5GFMAAAAoUg");
	this.shape_25.setTransform(142.25,123.6957);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#F6A500").s().p("AzRZQMAAAgqZQBZkNCPg+QB5hHBrg4IA+ggQCyhbDODKQD6D2EaATQCxAJC9hTQHdjbC6GdMAAAAqTg");
	this.shape_26.setTransform(142.25,116.4811);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#F6A500").s().p("AzRaYMAAAgsVQBRjmBvheQBjhhBfg6QAbgQAbgPQC6heDWDaQEEELElAOQC5AIDGhdQH5j0C6G1MAAAAsSg");
	this.shape_27.setTransform(142.25,109.2642);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#F6A500").s().p("AzRbgMAAAguRQFKtfG8HWQEPEfEwALQDBAGDOhoQIVkMC6HNMAAAAuRg");
	this.shape_28.setTransform(142.25,102.0537);

	this.instance_18 = new lib.Tween21("synched",0);
	this.instance_18.parent = this;
	this.instance_18.setTransform(142.25,102.05);
	this.instance_18._off = true;

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.instance_18];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.instance_18}]},81).to({state:[{t:this.instance_18}]},7).wait(3));
	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(109).to({_off:false},0).to({alpha:0},7).wait(3));

	// CROSS_STROKE
	this.instance_19 = new lib.Tween1("synched",0);
	this.instance_19.parent = this;
	this.instance_19.setTransform(149.2,126.35,0.8227,0.8227);

	this.timeline.addTween(cjs.Tween.get(this.instance_19).to({startPosition:0},118).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(147,159,156.7,156.7);
// library properties:
lib.properties = {
	id: 'A373688DE73E4875A41DE52935F1C1EA',
	width: 300,
	height: 300,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['A373688DE73E4875A41DE52935F1C1EA'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;