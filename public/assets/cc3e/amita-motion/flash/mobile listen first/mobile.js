(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.front = function() {
	this.initialize(img.front);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,414,800);


(lib.mob = function() {
	this.initialize(img.mob);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,464,800);


(lib.side = function() {
	this.initialize(img.side);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,328,796);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.mob();
	this.instance.parent = this;
	this.instance.setTransform(-232,-400);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-232,-400,464,800);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.front();
	this.instance.parent = this;
	this.instance.setTransform(-207,-400);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-207,-400,414,800);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.side();
	this.instance.parent = this;
	this.instance.setTransform(-164,-398);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-164,-398,328,796);


// stage content:
(lib.mobile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var This = this;
		
		window.gotoframe = function(i){
			This.gotoAndStop(i);
		}
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(50));

	// mobile_png
	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(209,360);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({startPosition:0},21).to({rotation:6.7025,x:369.35,y:375},6).wait(23));

	// mask_outside (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_1 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_2 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_3 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_4 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_5 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_6 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_7 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_8 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_9 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_10 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_11 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_12 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_13 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_14 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_15 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_16 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_17 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_18 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_19 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_20 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_21 = new cjs.Graphics().p("EhbZA+gMAAAh8/MC2zAAAMAAAB8/gAzPKtMAjKAAAMAAAg94MgjKAAAg");
	var mask_graphics_22 = new cjs.Graphics().p("EhcfA82MACPh89MC2wADSMgCPB89gAzbKXMAjJAAoMABIg93MgjJgAog");
	var mask_graphics_23 = new cjs.Graphics().p("EhdkA7LMAEfh85MC2qAGkMgEfB85gAzmKAMAjIABRMACOg91MgjIgBRg");
	var mask_graphics_24 = new cjs.Graphics().p("EhezA5LMAHJh8xMC2eAKcMgHJB8xgAzzJlMAjFACBMADig9xMgjFgCAg");
	var mask_graphics_25 = new cjs.Graphics().p("EhfzA3dMAJYh8nMC2PANuMgJYB8ngAz+JOMAjCACpMAEqg9rMgjDgCpg");
	var mask_graphics_26 = new cjs.Graphics().p("Ehg9A1ZMAMCh8XMC15ARmMgMCB8XgA0KIzMAi+ADZMAF9g9lMgi+gDYg");
	var mask_graphics_27 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_28 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_29 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_30 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_31 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_32 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_33 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_34 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_35 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_36 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_37 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_38 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_39 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_40 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_41 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_42 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_43 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_44 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_45 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_46 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_47 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_48 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");
	var mask_graphics_49 = new cjs.Graphics().p("EhiEAzaMAOmh8IMC1jAVVMgOmB8IgA0WIZMAi6AEGMAHOg9dMgi6gEGg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:201,y:397}).wait(1).to({graphics:mask_graphics_1,x:201,y:397}).wait(1).to({graphics:mask_graphics_2,x:201,y:397}).wait(1).to({graphics:mask_graphics_3,x:201,y:397}).wait(1).to({graphics:mask_graphics_4,x:201,y:397}).wait(1).to({graphics:mask_graphics_5,x:201,y:397}).wait(1).to({graphics:mask_graphics_6,x:201,y:397}).wait(1).to({graphics:mask_graphics_7,x:201,y:397}).wait(1).to({graphics:mask_graphics_8,x:201,y:397}).wait(1).to({graphics:mask_graphics_9,x:201,y:397}).wait(1).to({graphics:mask_graphics_10,x:201,y:397}).wait(1).to({graphics:mask_graphics_11,x:201,y:397}).wait(1).to({graphics:mask_graphics_12,x:201,y:397}).wait(1).to({graphics:mask_graphics_13,x:201,y:397}).wait(1).to({graphics:mask_graphics_14,x:201,y:397}).wait(1).to({graphics:mask_graphics_15,x:201,y:397}).wait(1).to({graphics:mask_graphics_16,x:201,y:397}).wait(1).to({graphics:mask_graphics_17,x:201,y:397}).wait(1).to({graphics:mask_graphics_18,x:201,y:397}).wait(1).to({graphics:mask_graphics_19,x:201,y:397}).wait(1).to({graphics:mask_graphics_20,x:201,y:397}).wait(1).to({graphics:mask_graphics_21,x:201,y:397}).wait(1).to({graphics:mask_graphics_22,x:226.8423,y:399.5775}).wait(1).to({graphics:mask_graphics_23,x:252.7411,y:402.155}).wait(1).to({graphics:mask_graphics_24,x:278.7859,y:404.1812}).wait(1).to({graphics:mask_graphics_25,x:304.8174,y:406.7643}).wait(1).to({graphics:mask_graphics_26,x:330.9674,y:408.7489}).wait(1).to({graphics:mask_graphics_27,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_28,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_29,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_30,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_31,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_32,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_33,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_34,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_35,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_36,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_37,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_38,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_39,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_40,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_41,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_42,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_43,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_44,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_45,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_46,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_47,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_48,x:357.0646,y:410.8078}).wait(1).to({graphics:mask_graphics_49,x:357.0646,y:410.8078}).wait(1));

	// front_png
	this.instance_1 = new lib.Tween2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(545,359);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regX:0.1,regY:0.1,rotation:4.0058,x:562.35,y:348.35},6).to({rotation:-7.2602,x:515.3,y:330.05},7).to({rotation:3.7288,x:553.55,y:331.1},5).to({regY:0.2,scaleX:1.1035,scaleY:1.1035,rotation:-38.6445,x:414.1,y:298.75},21).wait(11));

	// mask_screen (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_0 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_1 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_2 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_3 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_4 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_5 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_6 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_7 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_8 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_9 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_10 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_11 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_12 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_13 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_14 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_15 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_16 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_17 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_18 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_19 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_20 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_21 = new cjs.Graphics().p("ArjeTMAAAgu5MAi5AAAMAAAAu5g");
	var mask_1_graphics_22 = new cjs.Graphics().p("Ap5d/MAA2gu4MAi4AApMgA2Au3g");
	var mask_1_graphics_23 = new cjs.Graphics().p("AoPdsMABrgu2MAi4ABQMgBsAu2g");
	var mask_1_graphics_24 = new cjs.Graphics().p("AmsdTMACsguzMAi1AB/MgCsAuzg");
	var mask_1_graphics_25 = new cjs.Graphics().p("AlAc/MADhguwMAiyACoMgDhAuvg");
	var mask_1_graphics_26 = new cjs.Graphics().p("AjbclMAEgguqMAivADXMgEhAuqg");
	var mask_1_graphics_27 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_28 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_29 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_30 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_31 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_32 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_33 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_34 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_35 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_36 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_37 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_38 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_39 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_40 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_41 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_42 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_43 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_44 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_45 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_46 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_47 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_48 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");
	var mask_1_graphics_49 = new cjs.Graphics().p("Ah0cMMAFdgukMAirAEFMgFfAukg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:mask_1_graphics_0,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_1,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_2,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_3,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_4,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_5,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_6,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_7,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_8,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_9,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_10,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_11,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_12,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_13,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_14,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_15,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_16,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_17,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_18,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_19,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_20,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_21,x:149.4,y:193.85}).wait(1).to({graphics:mask_1_graphics_22,x:165.3096,y:195.9359}).wait(1).to({graphics:mask_1_graphics_23,x:181.1772,y:197.9988}).wait(1).to({graphics:mask_1_graphics_24,x:197.2847,y:200.2294}).wait(1).to({graphics:mask_1_graphics_25,x:213.1349,y:202.2368}).wait(1).to({graphics:mask_1_graphics_26,x:229.1671,y:204.3768}).wait(1).to({graphics:mask_1_graphics_27,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_28,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_29,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_30,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_31,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_32,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_33,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_34,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_35,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_36,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_37,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_38,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_39,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_40,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_41,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_42,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_43,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_44,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_45,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_46,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_47,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_48,x:245.1747,y:206.4805}).wait(1).to({graphics:mask_1_graphics_49,x:245.1747,y:206.4805}).wait(1));

	// side_png
	this.instance_2 = new lib.Tween1("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(540.05,290.55,0.6606,0.6606,0,0,0,0.1,0.1);

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({startPosition:0},18).to({regX:0.2,rotation:-37.943,x:360.9,y:246.8},21).to({regX:0.3,regY:0.3,scaleX:0.9769,scaleY:0.9769,rotation:0.0626,x:364.6,y:263.35},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(309.8,320.3,558.2,479.09999999999997);
// library properties:
lib.properties = {
	id: 'E2A813E9377A405884D8AD753BE49AD3',
	width: 700,
	height: 750,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/front.png?1559124789543", id:"front"},
		{src:"images/mob.png?1559124789543", id:"mob"},
		{src:"images/side.png?1559124789543", id:"side"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['E2A813E9377A405884D8AD753BE49AD3'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;